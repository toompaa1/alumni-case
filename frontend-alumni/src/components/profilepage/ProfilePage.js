import React, { useEffect, useState } from "react";
import "./profilepage.css";
import {
  Button,
  Modal,
  Table,
  Container,
  Image,
  Col,
  Row,
} from "react-bootstrap";
import { GetGroupsByCreatedUserId, GetEventByCreatedUserId } from "../../util/api";
import GroupPageFeed from "../grouppage/GroupPageFeed";
import { useHistory } from "react-router-dom";
import EventPageFeed from "../eventpage/EventPageFeed";
import withAuth from "../Hoc/Guarded";
import { useContext } from "react";
import { UserContext } from "../../context";

const Profile = () => {
  const [users, setUser] = useState([]);
  const [firstName, setName] = useState("");
  const [lastName, setLastName] = useState("");
  const [profilePicture, setProfilePicture] = useState("");
  const [bio, setBio] = useState("");
  const [funFact, setFunFact] = useState("");
  const [workStatus, setWorkStatus] = useState("");
  const [show, setShow] = useState(false);
  const ids = localStorage.getItem("id");
  const [userId, setUserId] = useState("");
  const [Groups, setGroups] = useState([]);
  const [myEvents, setMyEvents] = useState([]);
  const keycloak = useContext(UserContext);
  const history = useHistory();

  useEffect(() => {
    const getToken = () => {
      const token = localStorage.getItem("token");
      const bearer = "Bearer " + token;
      return bearer;
    };
    const getUsers = async () => {
      let Response = await fetch(
        `https://localhost:44385/api/users/${ids}`, {
        method: "GET",
        withCredentials: true,
        credentials: "include",
        headers: {
          Authorization: getToken(),
          "Content-Type": "application/json",
        },
      }
      );
      let data = await Response.json();
      let UserArr = [];
      UserArr.push(data);
      setUser(UserArr);
      setProfilePicture(UserArr[0].profilePicture);
      const myEvent = await GetEventByCreatedUserId(ids);
      setMyEvents(myEvent);
      const fetchedData = await GetGroupsByCreatedUserId(ids);
      setGroups(fetchedData);
    };
    getUsers();
  }, []);

  function selectUser() {
    let user = users[0];
    setName(user.firstName);
    setLastName(user.lastName);
    setProfilePicture(user.profilePicture);
    setBio(user.bio);
    setFunFact(user.funFact);
    setWorkStatus(user.workStatus);
    setUserId(user.userId);
    setShow(true);
  }

  const getToken = () => {
    const token = localStorage.getItem("token");
    const bearer = "Bearer " + token;
    return bearer;
  };

  const updateUser = async () => {
    const keycloakId = keycloak.idTokenParsed.sub;
    let item = {
      userId,
      firstName,
      lastName,
      profilePicture,
      bio,
      funFact,
      workStatus,
      keycloakId,
    };
    try {
      fetch(`https://localhost:44385/api/Users/${ids}`, {
        method: "PUT",
        mode: "cors",
        headers: {
          Authorization: getToken(),
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      });
    } catch (error) {
      console.log(error);
    }

    let newUsers = [];
    users.forEach((user) => {
      if (user.userId === userId) {
        newUsers.push(item);
      } else {
        newUsers.push(user);
      }
    });
    setUser(newUsers);
    setShow(false);
  };

  const handleClose = () => {
    setShow(false);
  };

  const viewGroup = (groupId) => {
    history.push(`/group/${groupId}`);
  };

  const viewEvent = (eventValue) => {
    history.push(`/event/${eventValue.eventId}`);
  };

  const groupRenderer = Groups.map((group, index) => {
    return (
      <GroupPageFeed key={index} groupValue={group} groupselected={viewGroup} />
    );
  });

  const createdEventRenderer = myEvents.map((event, index) => {
    return (
      <EventPageFeed key={index} eventValue={event} eventSelected={viewEvent} />
    );
  });

  const logout = () => {
    history.push(`/`);
    localStorage.clear();
    keycloak.logout();
  };

  return (
    <div className="ok">
      <div className="profile-button-container">
        <Button
          className="mx-3"
          variant="outline-light"
          onClick={() => selectUser(ids)}
        >
          Update profile
        </Button>
        <Button className="mx-3" variant="outline-light" onClick={logout}>
          Logout
        </Button>
      </div>
      <Container>
        <Col xs={1} md={1}>
          <Image className="my-3" src={profilePicture} roundedCircle />
        </Col>
      </Container>
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Bio</th>
            <th>FunFact</th>
            <th>WorkStatus</th>
          </tr>
        </thead>
        <tbody>
          {users.map((item, i) => (
            <tr key={i}>
              <td>{item.firstName}</td>
              <td>{item.lastName}</td>
              <td>{item.bio}</td>
              <td>{item.funFact}</td>
              <td>{item.workStatus}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      <div className="wrapper">
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Update your profile</Modal.Title>
          </Modal.Header>
          <Modal.Body className="test">
            <p>Firstname</p>
            <input
              type="text"
              value={firstName || ""}
              onChange={(e) => {
                setName(e.target.value);
              }}
            />
            <p>Lastname</p>
            <input
              type="text"
              value={lastName || ""}
              onChange={(e) => {
                setLastName(e.target.value);
              }}
            />
            <p>Profilepicture</p>
            <input
              type="text"
              value={profilePicture || ""}
              onChange={(e) => {
                setProfilePicture(e.target.value);
              }}
            />
            <p>Bio</p>
            <input
              type="text"
              value={bio || ""}
              onChange={(e) => {
                setBio(e.target.value);
              }}
            />
            <p>Funfact</p>
            <input
              type="text"
              value={funFact || ""}
              onChange={(e) => {
                setFunFact(e.target.value);
              }}
            />
            <p>Workstatus</p>
            <input
              type="text"
              value={workStatus || ""}
              onChange={(e) => {
                setWorkStatus(e.target.value);
              }}
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button onClick={updateUser}>Update User</Button>
          </Modal.Footer>
        </Modal>
      </div>
      <div className="Cards">
        <div className="groups">
          <h3 className="event-title"> My groups</h3>
          <Row> {groupRenderer} </Row>
        </div>
        <div className="events">
          <h3 className="event-title"> My events</h3>
          <Row> {createdEventRenderer}</Row>
        </div>
      </div>
    </div>
  );
};
export default withAuth(Profile);
