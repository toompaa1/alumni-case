import React from "react";
import "./profilepage.css";
import { useEffect, useState } from "react";
import { Table, Container, Image, Col } from "react-bootstrap";
import { useParams } from "react-router-dom";
import withAuth from "../Hoc/Guarded";

const ProfilePageProfile = () => {
  const [user, setUser] = useState([]);
  const [profilePicture, setProfilePicture] = useState("");
  const { id } = useParams();

  useEffect(() => {
    const getToken = () => {
      const token = localStorage.getItem("token");
      const bearer = "Bearer " + token;
      return bearer;
    };
    const getUser = async () => {
      let Response = await fetch(
        `https://localhost:44385/api/users/${id}`, {
        method: "GET",
        withCredentials: true,
        credentials: "include",
        headers: {
          Authorization: getToken(),
          "Content-Type": "application/json",
        },
      }
      );
      let data = await Response.json();
      let alumniUser = [data];
      setUser(alumniUser);
      setProfilePicture(alumniUser[0].profilePicture);
    };
    getUser();
  }, []);

  return (
    <div className="ok">
      <Container>
        <Col xs={1} md={1}>
          <Image className="my-3" src={profilePicture} roundedCircle />
        </Col>
      </Container>
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Bio</th>
            <th>FunFact</th>
            <th>WorkStatus</th>
          </tr>
        </thead>
        <tbody>
          {user.map((item, i) => (
            <tr key={i}>
              <td>{item.firstName}</td>
              <td>{item.lastName}</td>
              <td>{item.bio}</td>
              <td>{item.funFact}</td>
              <td>{item.workStatus}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default withAuth(ProfilePageProfile);
