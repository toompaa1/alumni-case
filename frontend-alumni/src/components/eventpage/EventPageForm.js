import './eventpage.css';
import { Modal, Button } from 'react-bootstrap';
import { useState } from 'react';

const EventPageForm = ({ show, createEventPost, hideModal }) => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  const create = () => {
    createEventPost(title, description);
  };

  const handleChangeDescription = (e) => {
    setDescription(e.target.value);
  };
  const handleChangeTitle = (e) => {
    setTitle(e.target.value);
  };

  return (
    <div>
      <Modal show={show} onHide={hideModal}>
        <Modal.Header closeButton>
          <Modal.Title>New Post</Modal.Title>
        </Modal.Header>
        <Modal.Body className="test">
          <input placeholder="Title" onChange={handleChangeTitle}></input>
          <textarea
            placeholder="Description"
            onChange={handleChangeDescription}
          ></textarea>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={hideModal}>
            Close
          </Button>
          <Button variant="primary" onClick={create}>
            Create
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default EventPageForm;
