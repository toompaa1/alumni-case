import './eventpage.css';
import { Modal, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { GetAllUsers } from '../../util/api';

const EventPageInviteForm = ({ showInviteForm, inviteMember, hideModal }) => {
  const [memberUserId, setMemberUserId] = useState(Number);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    async function fetchAllUsers() {
      const allUsers = await GetAllUsers()
      setUsers(allUsers)
    }
    fetchAllUsers();
  }, [])

  const invite = () => {
    inviteMember(memberUserId);
  };

  const handleChangeMemberUserId = (e) => {
    setMemberUserId(e.target.value);
  };

  const renderUsers = users.map((user, index) => {
    return (
      <option key={index} value={user.userId}>{user.firstName} {user.lastName}</option>
    )
  })

  return (
    <div>
      <Modal show={showInviteForm} onHide={hideModal}>
        <Modal.Header closeButton>
          <Modal.Title>Invite member</Modal.Title>
        </Modal.Header>
        <Modal.Body className="test">
          <select id="users" defaultValue={"Default"} onChange={handleChangeMemberUserId}>
            <option value="Default" disabled>Choose a user</option>
            {renderUsers}
          </select>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={hideModal}>
            Close
          </Button>
          <Button variant="primary" onClick={invite}>
            Invite
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default EventPageInviteForm;