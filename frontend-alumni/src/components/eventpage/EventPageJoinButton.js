import './eventpage.css';
import { Button } from 'react-bootstrap';

const JoinButton = ({ onPress }) => {
  return (
    <Button className="mx-3" variant="outline-light" onClick={onPress}>
      Join Event
    </Button>
  );
};

export default JoinButton;
