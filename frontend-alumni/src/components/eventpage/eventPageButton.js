import './eventpage.css';
import { Button } from 'react-bootstrap';

const CreateButton = ({ onPress }) => {
  return (
    <Button variant="outline-light" onClick={onPress}>
      New post
    </Button>
  );
};

export default CreateButton;
