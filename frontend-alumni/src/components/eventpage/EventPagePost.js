import './eventpage.css';
import { useParams } from 'react-router';
import { createPostComment, getCommentsByPostId, getPostById, getUserByUserId } from '../../util/api';
import EventPagePostComment from './EventPagePostComments';
import { useState, useEffect } from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import withAuth from '../Hoc/Guarded';

const EventPagePost = () => {
  const [post, setPost] = useState([]);
  const [comments, setComments] = useState([]);
  const { id } = useParams();
  const userId = localStorage.getItem('id');
  const [text, setText] = useState('');
  const [user, setUser] = useState();
  useEffect(() => {
    async function fetchData() {
      const post = await getPostById(id);
      const comments = await getCommentsByPostId(id);
      const user = await getUserByUserId(userId)
      setComments(comments);
      setPost(post);
      setUser(user);
    }
    fetchData();
  }, []);

  const commentRenderer = comments.map((comment, index) => {
    return <EventPagePostComment commentValue={comment} key={index} />;
  });

  const postComment = async (e) => {
    if (e.key === 'Enter') {
      let comment = await createPostComment(id, user.userId, text, user.firstName, user.lastName)
      e.target.value = "";
      setComments([...comments, comment]);
    }
  }

  const handleChange = (e) => {
    setText(e.target.value);
  }

  return (
    <div className="posts">
      <Row className="posts-row">
        <Col md="7">
          <Card
            bg="light"
            text='dark'
            className="mb-2"
          >
            <Card.Header>Published: {post.date} by {post.firstName} {post.lastName}</Card.Header>
            <Card.Body>
              <Card.Title>
                <div>
                  {post.title}
                </div>
              </Card.Title>
              <Card.Text>
                {post.description}
              </Card.Text>
            </Card.Body>
            <Card.Footer>
              {commentRenderer}
              <textarea onChange={handleChange} className="comment-input" placeholder="New comment" onKeyDown={postComment}></textarea>
            </Card.Footer>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default withAuth(EventPagePost);
