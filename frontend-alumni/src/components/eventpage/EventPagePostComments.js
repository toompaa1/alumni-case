import './eventpage.css';

const EventPagePostComment = ({ commentValue }) => {
  return (
    <div className="comment">
      <div className="comment-header"><p className="comment-user">{commentValue.firstName} {commentValue.lastName}</p><p className="comment-date">{commentValue.date}</p></div>
      <p>{commentValue.text}</p>
    </div>
  );
};

export default EventPagePostComment;
