import './eventpage.css';

const EventPagePosts = ({ postValue, postSelected }) => {
  const postClicked = () => {
    postSelected(postValue);
  };
  return (
    <div className="event-post" onClick={postClicked}>
      <h5>{postValue.title}</h5>
      <p>{postValue.description}</p>
      <p>Published: {postValue.date}</p>
      <p>Created by {postValue.firstName} {postValue.lastName}</p>
    </div>
  );
};

export default EventPagePosts;
