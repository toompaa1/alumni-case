import "./eventpage.css";
import { Button } from "react-bootstrap";

const InviteButton = ({ onPress }) => {
  return (
    <Button className="mx-3" variant="outline-light" onClick={onPress}>
      Invite member
    </Button>
  );
};

export default InviteButton;
