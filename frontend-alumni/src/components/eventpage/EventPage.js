import './eventpage.css';
import EventPageFeed from './EventPageFeed';
import { getAllEventsByUserId, GetEventByCreatedUserId } from '../../util/api';
import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Row } from 'react-bootstrap';
import withAuth from '../Hoc/Guarded';

const EventPage = () => {
  const userId = localStorage.getItem('id');
  const [events, setEvents] = useState([]);
  const [myEvents, setMyEvents] = useState([]);
  useEffect(() => {
    async function fetchData() {
      const fetchedData = await getAllEventsByUserId(userId);
      const myEvent = await GetEventByCreatedUserId(userId);
      let arr = [];
      arr.push(fetchedData)
      setEvents(arr.flat(Infinity));
      setMyEvents(myEvent)
    }
    fetchData();
  }, []);
  //test
  const history = useHistory();

  const viewEvent = (eventValue) => {
    history.push(`/event/${eventValue.eventId}`);
  };

  const eventRenderer = events.map((event, index) => {
    return (
      <EventPageFeed key={index} eventValue={event} eventSelected={viewEvent} />
    );
  });

  const createdEventRenderer = myEvents.map((event, index) => {
    return (
      <EventPageFeed key={index} eventValue={event} eventSelected={viewEvent} />
    );
  })

  return (
    <div className="event-page">
      <div className="event-page-container">
        <h3 className="event-title">Owner of events</h3>
        <Row>{createdEventRenderer}</Row>
      </div>
      <div className="event-page-container">
        <h3 className="event-title">Member of events</h3>
        <Row>{eventRenderer}</Row>
      </div>
    </div>
  );
};

export default withAuth(EventPage);
