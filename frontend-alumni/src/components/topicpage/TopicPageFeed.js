import "./topicpage.css";
import { Button, Card, Col } from "react-bootstrap";

const TopicPageFeed = ({ TopicValue, Topicselected }) => {
  const TopicClicked = () => {
    Topicselected(TopicValue.topicId);
  };

  return (
    <Col md="6" className="my-2">
      <Card className="text-center">
        <Card.Header>Topic</Card.Header>
        <Card.Body>
          <Card.Title>{TopicValue.name}</Card.Title>
          <Card.Text>{TopicValue.description}</Card.Text>
          <Button variant="primary" onClick={TopicClicked}>
            Go to topic
          </Button>
        </Card.Body>
      </Card>
    </Col>
  );
};

export default TopicPageFeed;
