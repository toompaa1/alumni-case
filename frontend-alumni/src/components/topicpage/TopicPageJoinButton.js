import "./topicpage.css";
import { Button } from "react-bootstrap";

const JoinButton = ({ onPress }) => {
  return (
    <Button className="mx-3" variant="outline-light" onClick={onPress}>
      Join topic
    </Button>
  );
};

export default JoinButton;
