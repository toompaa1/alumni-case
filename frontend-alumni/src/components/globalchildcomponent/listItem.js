import "./listItem.css";
import { ListGroup } from "react-bootstrap";
import { LockFill } from "react-bootstrap-icons";

const ListItem = ({ object, selectedItem }) => {
  const selected = () => {
    selectedItem(object);
  };
  return (
    <ListGroup.Item className="listItem" onClick={selected}>
      {object.name} {object.firstName} {object.lastName}{" "}
      {object.isPrivate === true ? <LockFill /> : ""}
    </ListGroup.Item>
  );
};

export default ListItem;
