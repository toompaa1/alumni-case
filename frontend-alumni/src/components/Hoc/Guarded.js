import { Redirect } from "react-router-dom";

function withAuth(Component) {
  return function () {
    if (localStorage.hasOwnProperty("token")) {
      return <Component />;
    } else {
      return <Redirect to="/" />;
    }
  };
}

export default withAuth;
