import { useState } from "react";
import { Modal, Button } from "react-bootstrap";

const StartPageEventForm = ({ hideModal, eventSubmitHandler, showEvent }) => {
  const [eventTitle, setEventTitle] = useState("");
  const [description, setDescription] = useState("");
  const [date, setDate] = useState("");
  const [isPrivate, setIsPrivate] = useState(false);

  const create = () => {
    const event = {
      Name: eventTitle,
      Description: description,
      IsPrivate: isPrivate,
      EndDate: date,
    };
    eventSubmitHandler(event);
  };

  const eventTextHandler = (event) => {
    setDescription(event.target.value);
  };

  const eventChangeHandler = (event) => {
    setEventTitle(event.target.value);
  };

  const handleIsPrivateChange = (event) => {
    event.target.value === "true" ? setIsPrivate(true) : setIsPrivate(false);
  };

  const handleDate = (event) => {
    setDate(event.target.value);
  };

  return (
    <div>
      <Modal show={showEvent} onHide={hideModal}>
        <Modal.Header closeButton>
          <Modal.Title>New event</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input placeholder="Title" onChange={eventChangeHandler}></input>
          <textarea
            placeholder="Description"
            onChange={eventTextHandler}
          ></textarea>
          <select value={isPrivate} onChange={handleIsPrivateChange}>
            <option value="true">Private</option>
            <option value="false">Public</option>
          </select>
          <input type="date" onChange={handleDate}></input>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={hideModal}>
            Close
          </Button>
          <Button variant="primary" onClick={create}>
            Create
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default StartPageEventForm;
