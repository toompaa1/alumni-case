import { useState } from "react";
import { Modal, Button } from "react-bootstrap";

const StartPageGroupForm = ({ groupSubmitHandler, showGroup, hideModal }) => {
  const [groupTitle, setGroupTitle] = useState("");
  const [description, setDescription] = useState("");
  const [isPrivate, setIsPrivate] = useState(false);

  const create = () => {
    const group = {
      Name: groupTitle,
      Description: description,
      IsPrivate: isPrivate,
    };

    groupSubmitHandler(group);
  };

  const groupTextHandler = (event) => {
    setDescription(event.target.value);
  };

  const groupChangeHandler = (event) => {
    setGroupTitle(event.target.value);
  };

  const handleIsPrivateChange = (event) => {
    event.target.value === "true" ? setIsPrivate(true) : setIsPrivate(false);
  };

  return (
    <div>
      <Modal show={showGroup} onHide={hideModal}>
        <Modal.Header closeButton>
          <Modal.Title>New group</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input placeholder="Title" onChange={groupChangeHandler}></input>
          <textarea
            placeholder="Description"
            onChange={groupTextHandler}
          ></textarea>
          <select value={isPrivate} onChange={handleIsPrivateChange}>
            <option value="true">Private</option>
            <option value="false">Public</option>
          </select>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={hideModal}>
            Close
          </Button>
          <Button variant="primary" onClick={create}>
            Create
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default StartPageGroupForm;
