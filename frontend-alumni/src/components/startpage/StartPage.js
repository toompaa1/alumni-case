import "./startpage.css";
import {
  getPostsByUserId,
  postTopic,
  postGroup,
  postEvent,
  getAllEvents,
  getAllGroups,
  getAllTopics,
  GetEventInventations,
  RespondToInvite,
  GetGroupInventations,
} from "../../util/api";
import { useEffect, useState } from "react";
import StartPageCreateTopicButton from "./StartPageCreateTopicButton";
import StartPageTopicForm from "./StartPageTopicForm";
import StartPageCreateGroupButton from "./StartPageCreateGroupButton";
import StartPageGroupForm from "./StartPageGroupForm";
import StartPageCreateEventButton from "./StartPageCreateEventButton";
import StartPageEventForm from "./StartPageEventForm";
import StartPageInputSearch from "./StartPageInputSearch";
import StartPageListValue from "./StartPageListvalue";
import { useHistory } from "react-router-dom";
import StartPagePosts from "../eventpage/EventPagePosts";
import { Row, Col, ListGroup } from "react-bootstrap";
import StartPageInviteList from "./StartPageInviteList";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const StartPage = () => {
  const [showGroupModal, setShowGroupModal] = useState(false);
  const [showEventModal, setShowEventModal] = useState(false);
  const [showTopicModal, setShowTopicModal] = useState(false);
  const [posts, setPosts] = useState([]);
  const [combinedValuesArray, setCombinedValuesArray] = useState([]);
  const [filterValueList, setFilterValueList] = useState([]);
  const [invites, setInvites] = useState([]);
  const history = useHistory();
  useEffect(() => {
    async function fetchdata(id) {
      const fetchedPosts = await getPostsByUserId(id);
      setPosts(fetchedPosts);
      const inviteEventList = await GetEventInventations(id);
      const inviteGroupList = await GetGroupInventations(id);
      const allEvents = await getAllEvents();
      const allGroups = await getAllGroups();
      const allTopics = await getAllTopics();
      const allInvites = [inviteEventList, inviteGroupList];
      const allEventsGroupsTopics = [allEvents, allTopics, allGroups];
      setCombinedValuesArray(allEventsGroupsTopics.flat(Infinity));
      setInvites(allInvites.flat(Infinity));
    }
    const localId = localStorage.getItem("id");
    fetchdata(localId);
  }, []);

  const handleCloseGroup = () => {
    setShowGroupModal(false);
  };

  const handleShowGroup = () => {
    setShowGroupModal(true);
  };

  const handleCloseEvent = () => {
    setShowEventModal(false);
  };

  const handleShowEvent = () => {
    setShowEventModal(true);
  };
  const handleCloseTopic = () => {
    setShowTopicModal(false);
  };

  const handleShowTopic = () => {
    setShowTopicModal(true);
  };

  const declineInviteClick = async (invite) => {
    const response = await RespondToInvite(
      invite.inviteId,
      2,
      invite.targetTypeId,
      invite.targetType
    );
    if (response === "Ok") {
      let inviteList = invites.filter(
        (inv) => inv.inviteId !== invite.inviteId
      );
      setInvites(inviteList);
    }
  };

  const handleClikOnInviteObj = (invite) => {
    if (invite.targetType === 0) {
      history.push(`/event/${invite.targetTypeId}`);
    } else if (invite.targetType === 1) {
      history.push(`/group/${invite.targetTypeId}`);
    }
  };

  const handleClickInvAccept = async (invite) => {
    const response = await RespondToInvite(
      invite.inviteId,
      1,
      invite.targetTypeId,
      invite.targetType
    );
    if (response === "Ok") {
      let inviteList = invites.filter(
        (inv) => inv.inviteId !== invite.inviteId
      );
      setInvites(inviteList);
      toast.success("Joined");
    }
  };

  const invitesList = invites.map((invite, index) => (
    <StartPageInviteList
      key={index}
      inv={invite}
      onInviteClick={handleClikOnInviteObj}
      acceptInviteClick={handleClickInvAccept}
      declineInviteClick={declineInviteClick}
    />
  ));

  const handelInputSearchOnChange = (input) => {
    if (input === "") {
      setFilterValueList([]);
    } else {
      let regexFilter = new RegExp(`^${input.toLowerCase()}`);
      const filterValues = combinedValuesArray.filter((x) =>
        x.name.toLowerCase().match(regexFilter)
      );
      setFilterValueList(filterValues);
    }
  };

  const handleClikedSearchValue = (value) => {
    //route to detailPage in the if statments
    if (value.hasOwnProperty("groupId")) {
      if (!value.isPrivate) {
        history.push(`/group/${value.groupId}`);
      }
    } else if (value.hasOwnProperty("topicId")) {
      if (!value.isPrivate) {
        history.push(`/topic/${value.topicId}`);
      }
    } else if (value.hasOwnProperty("eventId")) {
      if (!value.isPrivate) {
        history.push(`/event/${value.eventId}`);
      }
    }
  };

  const searchList = filterValueList.map((x, index) => (
    <StartPageListValue
      key={index}
      valueObject={x}
      handleLiClicked={handleClikedSearchValue}
    />
  ));

  const inputSearch = (
    <StartPageInputSearch handleInputChange={handelInputSearchOnChange} />
  );

  const handlePostClick = async (post) => {
    //Send to postDetailPage
    history.push(`/post/${post.postId}`);
  };

  const postsRenderer = posts.map((post, index) => (
    <StartPagePosts
      key={index}
      postValue={post}
      postSelected={handlePostClick}
    />
  ));

  const handleTopicSubmit = async (topic) => {
    const createdTopic = await postTopic(topic);
    setCombinedValuesArray([...combinedValuesArray, createdTopic]);
    setShowTopicModal(false);
    toast.success(`New topic: ${topic.Name}`);
  };

  const topicForm = (
    <StartPageTopicForm
      topicSubmitHandler={handleTopicSubmit}
      hideModal={handleCloseTopic}
      showTopic={showTopicModal}
    />
  );

  const handleGroupSubmit = async (group) => {
    const createdGroup = await postGroup(group);
    setCombinedValuesArray([...combinedValuesArray, createdGroup]);
    setShowGroupModal(false);
    toast.success(`New group: ${group.Name}`);
  };

  const groupForm = (
    <StartPageGroupForm
      groupSubmitHandler={handleGroupSubmit}
      hideModal={handleCloseGroup}
      showGroup={showGroupModal}
    />
  );

  const handleEventSubmit = async (event) => {
    const createdEvent = await postEvent(event);
    setCombinedValuesArray([...combinedValuesArray, createdEvent]);
    setShowEventModal(false);
    toast.success(`New event: ${event.Name}`);
  };
  const eventForm = (
    <StartPageEventForm
      eventSubmitHandler={handleEventSubmit}
      hideModal={handleCloseEvent}
      showEvent={showEventModal}
    />
  );

  return (
    <div>
      <Row className="members-group-container my-4">
        <Col md="5">
          <Col md="6" className="my-3">
            <StartPageCreateGroupButton onPress={handleShowGroup} />
          </Col>
          <Col md="6" className="my-3">
            <StartPageCreateEventButton onPress={handleShowEvent} />
          </Col>
          <Col md="6" className="my-3">
            <StartPageCreateTopicButton onPress={handleShowTopic} />
          </Col>
          <Col md="6" className="my-3">
            <h5>Search</h5>
            {inputSearch}
            <ListGroup>{searchList}</ListGroup>
          </Col>
          <Col md="8" className="my-3">
            <h5>Pending invites</h5>
            {invitesList}
          </Col>
        </Col>
        <Col md="7">
          <h5 className="mb-3">POSTS</h5>
          {postsRenderer}
        </Col>
      </Row>
      <h3>Invites:</h3>
      {topicForm}
      {groupForm}
      {eventForm}
      <ToastContainer />
    </div>
  );
};

export default StartPage;
