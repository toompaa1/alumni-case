import "./startpage.css";
import { Button } from "react-bootstrap";

const StartPageCreateEventButton = ({ onPress }) => {
  return (
    <div>
      <Button
        variant="outline-light"
        className="create-startpage-button"
        onClick={onPress}
      >
        Create Event
      </Button>
    </div>
  );
};

export default StartPageCreateEventButton;
