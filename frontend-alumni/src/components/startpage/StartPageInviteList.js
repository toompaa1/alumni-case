import { useState, useEffect } from "react";

const StartPageInviteList = ({
  inv,
  onInviteClick,
  acceptInviteClick,
  declineInviteClick,
}) => {
  const [type, setType] = useState("");

  useEffect(() => {
    let typeValue = inv.targetType === 0 ? "Event" : "Group";
    setType(typeValue);
  }, []);

  const handleClick = () => {
    onInviteClick(inv);
  };

  const handleAccept = () => {
    acceptInviteClick(inv);
  };

  const handleDecline = () => {
    declineInviteClick(inv);
  };

  return (
    <div className="invite-container">
      <div className="invite-detail">
        <p className="invite-p go-to-invite-type" onClick={handleClick}>
          New invite from {inv.invitedUserName} <br></br>
          {type}: {inv.name}
        </p>
      </div>
      <div>
        <p className="invite-p choice" onClick={handleAccept}>
          Accept
        </p>
        <p className="invite-p choice" onClick={handleDecline}>
          Decline
        </p>
      </div>
    </div>
  );
};

export default StartPageInviteList;
