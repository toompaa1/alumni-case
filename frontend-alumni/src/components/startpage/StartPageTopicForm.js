import "./startpage.css";
import { useState } from "react";
import { Modal, Button } from "react-bootstrap";

const StartPageTopicForm = ({ topicSubmitHandler, showTopic, hideModal }) => {
  const [topicTitle, setTopicTitle] = useState("");
  const [description, setDescription] = useState("");

  const create = () => {
    const topic = {
      Name: topicTitle,
      Description: description,
    };
    topicSubmitHandler(topic);
  };

  const topicTextHandler = (event) => {
    setDescription(event.target.value);
  };

  const topicChangeHandler = (event) => {
    setTopicTitle(event.target.value);
  };

  return (
    <div>
      <Modal show={showTopic} onHide={hideModal}>
        <Modal.Header closeButton>
          <Modal.Title>New topic</Modal.Title>
        </Modal.Header>
        <Modal.Body className="test">
          <input placeholder="Title" onChange={topicChangeHandler}></input>
          <textarea
            placeholder="Description"
            onChange={topicTextHandler}
          ></textarea>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={hideModal}>
            Close
          </Button>
          <Button variant="primary" onClick={create}>
            Create
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default StartPageTopicForm;
