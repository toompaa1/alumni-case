import "./startpage.css";
import { Button } from "react-bootstrap";

const StartPageCreateTopicButton = ({ onPress }) => {
  return (
    <div>
      <Button
        variant="outline-light"
        className="create-startpage-button"
        onClick={onPress}
      >
        Create Topic
      </Button>
    </div>
  );
};

export default StartPageCreateTopicButton;
