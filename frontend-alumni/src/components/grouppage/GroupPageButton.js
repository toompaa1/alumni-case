import "./grouppage.css";
import { Button } from "react-bootstrap";

const createButton = ({ onPress }) => {
  return (
    <Button variant="outline-light" onClick={onPress}>
      Create Post
    </Button>
  );
};

export default createButton;
