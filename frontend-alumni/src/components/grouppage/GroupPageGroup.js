import { useState, useEffect } from "react";
import {
  getPostInGroupByGroupId,
  getAllUsersInGroupByGroupId,
  getAllEventsInGroupByGroupId,
  GetGroupByIdGroupId,
  createPost,
  JoinGroup,
  getUserByUserId,
  InviteGroupMember,
} from "../../util/api";
import GroupPageButton from "./GroupPageButton.js";
import { useParams, useHistory } from "react-router";
import GroupPageForm from "./GroupPageForm.js";
import { Breadcrumb, Row, Col, ListGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import GroupPagePosts from "../eventpage/EventPagePosts";
import JoinButton from "./GroupPageJoinButton";
import ListItem from "../globalchildcomponent/listItem";
import InviteButton from "./GroupPageInviteButton";
import GroupPageInviteForm from "./GroupPageInviteForm";
import withAuth from "../Hoc/Guarded";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const GroupPageGroup = () => {
  const userId = parseInt(localStorage.getItem("id"));
  const [Group, setGroup] = useState([]);
  const [GroupPost, setGroupsPost] = useState([]);
  const [Users, setUsers] = useState([]);
  const [Events, setEvents] = useState([]);
  const { id } = useParams();
  const [show, setShow] = useState(false);
  const [showInviteModal, setShowInviteModal] = useState(false);
  const history = useHistory();

  useEffect(() => {
    async function fetchData() {
      // fetch group
      const fetchedDataGroup = await GetGroupByIdGroupId(id);
      setGroup(fetchedDataGroup);

      // fetch posts in group
      const fetchedDataPosts = await getPostInGroupByGroupId(id);
      setGroupsPost(fetchedDataPosts);

      // fetch users in a group
      const users = await getAllUsersInGroupByGroupId(id);
      setUsers(users);

      // fetch events that belongs to a group
      const events = await getAllEventsInGroupByGroupId(id);
      setEvents(events);
    }
    fetchData();
  }, []);

  const handleClose = () => {
    setShow(false);
  };

  const handleShow = () => {
    setShow(true);
  };

  const handleCloseInvite = () => {
    setShowInviteModal(false);
  };

  const handleShowInvite = () => {
    setShowInviteModal(true);
  };

  const createGroupPost = async (title, description) => {
    const post = await createPost(title, 1, userId, description, id);
    post.firstName = localStorage.getItem("firstName");
    post.lastName = localStorage.getItem("lastName");
    setGroupsPost([...GroupPost, post]);
    setShow(false);
  };

  const handleOnclickJoinButton = async () => {
    const userId = localStorage.getItem("id");
    const response = await JoinGroup(id, userId);
    if (response === "user added") {
      const user = await getUserByUserId(userId);
      setUsers([...Users, user]);
    }
  };

  const exist = () => {
    let check = false;
    Users.forEach((user) => {
      if (user.userId === userId) {
        check = true;
      }
    });
    return check;
  };

  const renderJoinButton = () => {
    if (exist()) {
      return;
    }
    return <JoinButton onPress={handleOnclickJoinButton} />;
  };

  const viewPost = (postValue) => {
    history.push(`/post/${postValue.postId}`);
  };

  const PostRenderer = GroupPost.map((groupPost, index) => {
    return (
      <GroupPagePosts
        postValue={groupPost}
        key={index}
        postSelected={viewPost}
      />
    );
  });

  const handleClickUser = (user) => {
    history.push(`/user/${user.userId}`);
  };

  const UserRender = Users.map((users, index) => {
    return (
      <ListItem
        object={users}
        key={index}
        selectedItem={handleClickUser}
      ></ListItem>
    );
  });

  const handleClickEvent = (event) => {
    if (!event.isPrivate) {
      history.push(`/event/${event.eventId}`);
    }
  };

  const renderEvent = Events.map((events, index) => {
    return (
      <ListItem
        object={events}
        key={index}
        selectedItem={handleClickEvent}
      ></ListItem>
    );
  });

  const checkIfOwner = () => {
    let check = false;
    if (Group.userId === userId) {
      check = true;
    }
    return check;
  };

  const inviteMember = async (userTargetId) => {
    const response = await InviteGroupMember(userId, userTargetId, id);
    setShowInviteModal(false);
    if (response === "Can not invite yourself") {
      toast.error(response);
      setShowInviteModal(false);
    } else if (
      response ===
      "This invite is pending or the user has already joined the group."
    ) {
      toast.error(response);
      setShowInviteModal(false);
    } else {
      toast.success("Invited");
      setShowInviteModal(false);
    }
  };

  const renderInviteButton = () => {
    if (!checkIfOwner()) {
      return;
    } else {
      return <InviteButton onPress={handleShowInvite} />;
    }
  };

  return (
    <div>
      <Breadcrumb>
        <Breadcrumb.Item linkAs={Link} linkProps={{ to: "/" }}>
          Alumni
        </Breadcrumb.Item>
        <Breadcrumb.Item linkAs={Link} linkProps={{ to: "/groups" }}>
          Groups
        </Breadcrumb.Item>
        <Breadcrumb.Item active>{id}</Breadcrumb.Item>
      </Breadcrumb>
      <div className="event-eventpage-feed">
        <div className="event-info-container">
          <h5>{Group.name}</h5>
          <p className="description">{Group.description}</p>
        </div>
        <div className="create-button-container">
          {renderJoinButton()} {renderInviteButton()}
          <GroupPageButton onPress={handleShow} />
        </div>
      </div>
      <Row className="members-group-container my-4">
        <Col md="5">
          <Col md="6" className="my-3">
            <ListGroup>
              <h5>Members</h5>
              {UserRender}
            </ListGroup>
          </Col>
          <Col md="6" className="my-3">
            <ListGroup>
              <h5>Events</h5>
              {renderEvent}
            </ListGroup>
          </Col>
        </Col>
        <Col md="7">
          <h5 className="mb-3">POSTS</h5>
          <div>{PostRenderer}</div>
        </Col>
      </Row>
      <GroupPageForm
        show={show}
        createGroupPost={createGroupPost}
        hideModal={handleClose}
      />
      <GroupPageInviteForm
        showInviteForm={showInviteModal}
        inviteMember={inviteMember}
        hideModal={handleCloseInvite}
      />
      <ToastContainer />
    </div>
  );
};

export default withAuth(GroupPageGroup);
