const API_URL = "https://localhost:44385/api/";
// const API_URL = "https://alumniapi.azurewebsites.net/api/";

const checkIfIdExist = () => {
  if (localStorage.hasOwnProperty("id")) {
    return localStorage.getItem("id");
  } else {
    return false;
  }
};

const getToken = () => {
  const token = localStorage.getItem("token");
  const bearer = "Bearer " + token;
  return bearer;
};

export async function getPostsByUserId(id) {
  try {
    const response = await fetch(`${API_URL}posts/${id}`, {
      method: "GET",
      withCredentials: true,
      credentials: "include",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    if (response.status === 401) {
      return [{ title: "Not Authorized" }];
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    return ["Not Authorized"];
  }
}

export async function getCommentsByPostId(id) {
  try {
    const response = await fetch(`${API_URL}comments/post/${id}`, {
      method: "GET",
      withCredentials: true,
      credentials: "include",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error, "Failed to load data from DB!");
  }
}

export async function postTopic(topic) {
  let userId = 0;
  if (!checkIfIdExist) {
    console.log("no id");
  } else {
    userId = checkIfIdExist();
  }

  try {
    const response = await fetch(`${API_URL}topics/${userId}`, {
      method: "POST",
      withCredentials: true,
      credentials: "include",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(topic),
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function postGroup(group) {
  let userId = 0;
  if (!checkIfIdExist) {
    console.log("no id");
  } else {
    userId = checkIfIdExist();
  }

  try {
    const response = await fetch(`${API_URL}group/user/${userId}`, {
      method: "POST",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(group),
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function getAlltopicsCreatedByUserId(userId) {
  try {
    const response = await fetch(
      `${API_URL}Topics/${userId}/GetCreatedTopicsByUserId`,
      {
        method: "GET",
        mode: "cors",
        headers: {
          Authorization: getToken(),
          "Content-Type": "application/json",
        },
      }
    );
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function getAllMembersInTopic(topicId) {
  try {
    const response = await fetch(`${API_URL}Topics/${topicId}/users`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function postEvent(event) {
  let date = new Date();
  let newDate =
    date.getFullYear() +
    "-" +
    ("0" + (date.getMonth() + 1)).slice(-2) +
    "-" +
    ("0" + date.getDate()).slice(-2);
  let eventObj = {
    Name: event.Name,
    Description: event.Description,
    IsPrivate: event.IsPrivate,
    StartDate: newDate,
    EndDate: event.EndDate,
  };
  let userId = 0;
  if (!checkIfIdExist) {
    console.log("no id");
  } else {
    userId = checkIfIdExist();
  }
  try {
    const response = await fetch(`${API_URL}events/user/${userId}`, {
      method: "POST",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(eventObj),
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function getAllEvents() {
  try {
    const response = await fetch(`${API_URL}events`, {
      method: "GET",
      withCredentials: true,
      credentials: "include",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    return [];
  }
}

export async function getAllGroups() {
  try {
    const response = await fetch(`${API_URL}group`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    return [];
  }
}

export async function getAllTopics() {
  try {
    const response = await fetch(`${API_URL}topics`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    return [];
  }
}

export async function getAllEventsByUserId(userId) {
  try {
    const response = await fetch(`${API_URL}events/${userId}/event`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function createPost(
  title,
  targetType,
  userId,
  description,
  targetId
) {
  let date = new Date();
  let newDate =
    date.getFullYear() +
    "-" +
    ("0" + (date.getMonth() + 1)).slice(-2) +
    "-" +
    ("0" + date.getDate()).slice(-2);

  let post = {
    title: title,
    date: newDate,
    lastUpdated: newDate,
    targetType: targetType,
    createdByUserId: userId,
    targetId: targetId,
    description: description,
  };
  try {
    const response = await fetch(`${API_URL}posts`, {
      method: "POST",
      mode: "cors",
      cache: "no-cache",
      credentials: "same-origin",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(post),
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error, "Failed to add new post!");
  }
}

export async function getAllPostsInEvent(eventId) {
  try {
    const response = await fetch(`${API_URL}posts/event/${eventId}`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error, "Failed to load data from DB!");
  }
}

export async function getEventById(eventId) {
  try {
    const response = await fetch(`${API_URL}events/${eventId}`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error, "Failed to load data from DB!");
  }
}

export async function getPostById(postId) {
  try {
    const response = await fetch(`${API_URL}posts/post/${postId}`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error, "Failed to load data from DB!");
  }
}

export async function getAllUsersInEventByEventId(eventId) {
  try {
    const response = await fetch(`${API_URL}events/${eventId}/users`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error, "Failed to load data from DB!");
  }
}

export async function getAllGroupsInEventByEventId(eventId) {
  try {
    const response = await fetch(`${API_URL}events/${eventId}/groups`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error, "Failed to load data from DB!");
  }
}

export async function getAllTopicsInEventByEventId(eventId) {
  try {
    const response = await fetch(`${API_URL}events/${eventId}/topics`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error, "Failed to load data from DB!");
  }
}

export async function getUserByKeycloakId(keycloakId) {
  try {
    const response = await fetch(`${API_URL}users/keycloak/${keycloakId}`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = response.status === 404 ? "Not Found" : await response.json();
    console.log(data);
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function postUser(keycloakUser) {
  const user = {
    firstName: keycloakUser.firstName,
    lastName: keycloakUser.lastName,
    profilePictire: "",
    bio: "",
    funFact: "",
    workStatus: "",
    keycloakId: keycloakUser.keycloakId,
  };

  try {
    const response = await fetch(`${API_URL}users`, {
      method: "POST",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(user),
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function getAllGroupByUserId(userId) {
  try {
    const response = await fetch(
      `${API_URL}Group/${userId}/AllRelevantGroups`,
      {
        method: "GET",
        mode: 'cors',
        headers: {
          Authorization: getToken(),
          "Content-Type": "application/json",
        },
      }
    );
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function getAllGroupByGroupId(GroupId) {
  try {
    const response = await fetch(`${API_URL}Group/${GroupId}`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function getPostInGroupByGroupId(GroupId) {
  try {
    const response = await fetch(`${API_URL}Posts/group/${GroupId}`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function getAllUsersInGroupByGroupId(GroupId) {
  try {
    const response = await fetch(`${API_URL}Group/${GroupId}/users`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function getAllEventsInGroupByGroupId(GroupId) {
  try {
    const response = await fetch(`${API_URL}group/${GroupId}/events`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    return [];
  }
}

export async function GetGroupByIdGroupId(GroupId) {
  try {
    const response = await fetch(`${API_URL}Group/${GroupId}`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function createPostComment(
  postId,
  userId,
  comment,
  firstName,
  lastName
) {
  let date = new Date();
  let newDate =
    date.getFullYear() +
    "-" +
    ("0" + (date.getMonth() + 1)).slice(-2) +
    "-" +
    ("0" + date.getDate()).slice(-2);

  let commentObj = {
    postId: postId,
    userId: userId,
    text: comment,
    date: newDate,
    firstName: firstName,
    lastName: lastName,
  };
  try {
    const response = await fetch(`${API_URL}Comments`, {
      method: "POST",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(commentObj),
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function JoinEvent(eventId, userId) {
  const eventJoin = {
    EventId: eventId,
    UserId: userId,
  };
  try {
    const response = await fetch(`${API_URL}events/join`, {
      method: "POST",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(eventJoin),
    });
    if (response.status === 404) {
      return "topic does not exist";
    } else if (response.status === 400) {
      return "Already a member";
    } else if (response.status === 200) {
      return "user added";
    }
  } catch (error) { }
}

export async function JoinTopic(topicId, userId) {
  const topicJoin = {
    topicId: topicId,
    userId: userId,
  };
  try {
    const response = await fetch(`${API_URL}topics/join`, {
      method: "POST",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(topicJoin),
    });
    if (response.status === 404) {
      return "topic does not exist";
    } else if (response.status === 400) {
      return "Already a member";
    } else if (response.status === 200) {
      return "user added";
    }
  } catch (error) { }
}

export async function JoinGroup(groupId, userId) {
  const groupJoin = {
    GroupId: groupId,
    UserId: userId,
  };
  try {
    const response = await fetch(`${API_URL}group/join`, {
      method: "POST",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(groupJoin),
    });
    if (response.status === 404) {
      return "topic does not exist";
    } else if (response.status === 400) {
      return "Already a member";
    } else if (response.status === 200) {
      return "user added";
    }
  } catch (error) { }
}

export async function getUserByUserId(id) {
  try {
    const response = await fetch(`${API_URL}Users/${id}`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function getAllTopicsByUserid(userId) {
  try {
    const response = await fetch(
      `${API_URL}topics/${userId}/AllRelevantTopics`,
      {
        method: "GET",
        mode: "cors",
        headers: {
          Authorization: getToken(),
          "Content-Type": "application/json",
        },
      }
    );
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    return [];
  }
}

export async function getAllTopicsById(TopicId) {
  try {
    const response = await fetch(`${API_URL}Topics/${TopicId}`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    return [];
  }
}

export async function getAllPostInTopicByTopicId(TopicId) {
  try {
    const response = await fetch(`${API_URL}Posts/Topic/${TopicId}`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    return [];
  }
}

export async function GetAllEventsByTopicId(TopicId) {
  try {
    const response = await fetch(`${API_URL}Events/${TopicId}/eventsInTopic`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    return [];
  }
}

export async function GetEventByCreatedUserId(userId) {
  try {
    const response = await fetch(`${API_URL}Events/createdBy/${userId}`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    return [];
  }
}

export async function GetAllUsers() {
  try {
    const response = await fetch(`${API_URL}Users`, {
      method: "GET",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}

export async function InviteEventMember(senderId, targetUserId, eventId) {
  let member = {
    createdUserId: senderId,
    targetUserId: targetUserId,
    status: 0,
    targetType: 0,
    targetTypeId: eventId,
  };
  try {
    const response = await fetch(`${API_URL}Events/invite`, {
      method: "POST",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(member),
    });
    if (response.status === 400) {
      return await response.text();
    } else {
      const data = await response.json();
      return data;
    }
  } catch (error) {
    console.log(error);
  }
}

export async function InviteGroupMember(senderId, targetUserId, groupId) {
  let member = {
    createdUserId: senderId,
    targetUserId: targetUserId,
    status: 0,
    targetType: 1,
    targetTypeId: groupId,
  };
  try {
    const response = await fetch(`${API_URL}Group/invite`, {
      method: "POST",
      mode: "cors",
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(member),
    });
    if (response.status === 400) {
      return await response.text();
    } else {
      const data = await response.json();
      return data;
    }
  } catch (error) {
    console.log(error);
  }
}

export async function GetEventInventations(userId) {
  try {
    const response = await fetch(
      `${API_URL}events/checkInviteRequest/${userId}`,
      {
        method: "GET",
        mode: "cors",
        headers: {
          Authorization: getToken(),
          "Content-Type": "application/json",
        },
      }
    );
    const data = await response.json();
    return data;
  } catch (error) { }
}

export async function RespondToInvite(
  inviteId,
  answer,
  targetTypeId,
  targetType
) {
  //only for events, do same for groups
  try {
    const respondInvite = {
      inviteId: inviteId,
      TargetTypeId: targetTypeId,
      Status: answer,
    };
    targetType = targetType === 0 ? "Events" : "Group";
    const response = await fetch(
      `${API_URL}${targetType}/respondToInvite/${inviteId}`,
      {
        method: "PUT",
        mode: "cors",
        headers: {
          Authorization: getToken(),
          "Content-Type": "application/json",
        },
        body: JSON.stringify(respondInvite),
      }
    );
    if (response.status === 204 || 200) {
      return "Ok";
    }
  } catch (error) { }
}

export async function GetGroupsByCreatedUserId(userId) {
  try {
    const response = await fetch(
      `${API_URL}Group/${userId}/GetCreatedGroupsByUserId`,
      {
        method: "GET",
        mode: "cors",
        headers: {
          Authorization: getToken(),
          "Content-Type": "application/json",
        },
      }
    );
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    return [];
  }
}

export async function GetGroupInventations(userId) {
  try {
    const response = await fetch(
      `${API_URL}group/checkInviteRequest/${userId}`,
      {
        method: "GET",
        mode: "cors",
        headers: {
          Authorization: getToken(),
          "Content-Type": "application/json",
        },
      }
    );
    const data = await response.json();
    return data;
  } catch (error) { }
}
