﻿using alumni.Models.Domain;
using alumni.Models.Dto.Comment;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Profiles
{
    public class CommentProfile: Profile
    {
        public CommentProfile()
        {
            CreateMap<Comment, CommentReadDTO>().ReverseMap();
            CreateMap<Comment, CommentCreateDTO>().ReverseMap();

        }
       

    }
}
