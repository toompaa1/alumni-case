﻿using alumni.Models.Domain;
using alumni.Models.Dto.Invite;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Profiles
{
    public class InviteProfile : Profile
    {
        public InviteProfile()
        {
            CreateMap<Invite, InviteCreateDTO>().ReverseMap();
            CreateMap<Invite, InviteReadDTO>().ReverseMap();
         
        }
    }
}
