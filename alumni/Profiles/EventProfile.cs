﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models.Domain;
using alumni.Models.Domain.EF_ManyToMany;
using alumni.Models.Dto.Event;
using AutoMapper;

namespace alumni.Profiles
{
    public class EventProfile : Profile
    {
        public EventProfile()
        {
            CreateMap<Event, EventReadDTO>();
            CreateMap<EventCreateDTO, Event>();
            CreateMap<EventUpdateDTO, Event>();
            CreateMap<Event, EventGroupDTO>()
                .ForMember(edto => edto.Groups, opt => opt
                .MapFrom(g => g.EventGroups.Select(g => g.GroupId)));
            CreateMap<Event, EventTopicDTO>()
                .ForMember(edto => edto.Topics, opt => opt
                .MapFrom(t => t.EventTopics.Select(t => t.TopicId)));
            CreateMap<Event, EventUserDTO>()
                .ForMember(edto => edto.Users, opt => opt
                .MapFrom(u => u.EventUsers.Select(u => u.UserId)));
            CreateMap<EventUser, EventReadDTO>();
            CreateMap<EventJoinDTO, EventUser>().ReverseMap();
        }
    }
}
