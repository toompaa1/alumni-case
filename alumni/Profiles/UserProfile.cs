﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models.Domain;
using alumni.Models.Dto.User;
namespace alumni.Profiles
{
    public class UserProfile : Profile

    {
        public UserProfile()
        {
            CreateMap<User, UserReadDTO>();
            CreateMap<User, UserCommentDTO>()
                .ForMember(x => x.Comments, opt => opt
                .MapFrom(t => t.Comments
                .Select(t => t.CommentId).ToList()));
            CreateMap<UserCreateDTO, User>();
            CreateMap<UserUpdateDTO, User>();
        }
    }
}
