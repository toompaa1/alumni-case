﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models.Domain;
using alumni.Models.Dto.Topic;
using alumni.Models.Domain.EF_ManyToMany;

namespace alumni.Profiles
{
    public class TopicProfile : Profile
    {
        public TopicProfile()
        {
            CreateMap<Topic, TopicReadDTO>();
            CreateMap<TopicCreateDTO, Topic>();
            CreateMap<TopicUser, JoinTopicDTO>().ReverseMap();
        }
    }
}
