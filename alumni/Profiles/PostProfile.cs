﻿using alumni.Models.Domain;
using alumni.Models.Dto.Post;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Profiles
{
    public class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<Post, PostReadDto>().ReverseMap();

            CreateMap<PostCreateDTO, Post>();

            CreateMap<Post, PostUpdateDTO>().ReverseMap();
        }
    }
}