﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models.Domain;
using alumni.Models.Domain.EF_ManyToMany;
using alumni.Models.Dto.Group;
using AutoMapper;

namespace alumni.Profiles
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
            CreateMap<Group, GroupReadDTO>();
            CreateMap<GroupCreateDTO, Group>();
            CreateMap<Group, GroupUserDTO>()
             .ForMember(gdto => gdto.Users, opt => opt
             .MapFrom(u => u.GroupUsers.Select(u => u.UserId)));
            //CreateMap<Group, GroupEventDTO>()
            // .ForMember(gdto => gdto.Events, opt => opt
            // .MapFrom(u => u.GroupEvents.Select(u => u.EventId)));
            CreateMap<GroupJoinDTO, GroupUser>();
        }
    }
}
