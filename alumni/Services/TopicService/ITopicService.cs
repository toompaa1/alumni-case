﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models.Domain;
using alumni.Models.Domain.EF_ManyToMany;

namespace alumni.Services.TopicService
{
    public interface ITopicService
    {
        public Task<IEnumerable<Topic>> GetAllTopics();
        public Task<Topic> GetTopicById(int topicId);
        public Task<Topic> CreateTopic(Topic topic, int userId);
        public Task<IEnumerable<Topic>> GetTopicByUserId(int userId);
        public Task JoinTopic(TopicUser topicuser);
        public bool UserExistsInTopic(int userId, int topicId);
        public bool TopicExist(int topicId);
        public Task<IEnumerable<Topic>> GetCreatedTopics(int userId);
        public Task<IEnumerable<User>> GetAllTopicUsers(int topicId);
    }
}
