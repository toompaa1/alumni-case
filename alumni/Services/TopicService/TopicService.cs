﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models;
using alumni.Models.Domain;
using alumni.Models.Domain.EF_ManyToMany;
using Microsoft.EntityFrameworkCore;

namespace alumni.Services.TopicService
{
    public class TopicService : ITopicService
    {
        private readonly AlumniDbContext _context;
        public TopicService(AlumniDbContext alumniDbContext)
        {
            _context = alumniDbContext;
        }

        /// <summary>
        /// Method to get all topics from DB.
        /// </summary>
        /// <returns>List of topics</returns>
        public async Task<IEnumerable<Topic>> GetAllTopics()
        {
            return await _context.Topics.Include(e => e.EventTopics).ToListAsync();
        }

        /// <summary>
        /// Method to get a topic with specific given ID.
        /// </summary>
        /// <param name="topicId">ID of topic</param>
        /// <returns>One specific topic</returns>
        public async Task<Topic> GetTopicById(int topicId)
        {
            return await _context.Topics.FindAsync(topicId);
        }

        /// <summary>
        /// Method to create a topic.
        /// </summary>
        /// <param name="topic">Takes a topic object.</param>
        /// <returns>Created object</returns>
        public async Task<Topic> CreateTopic(Topic topic, int userId)
        {
            _context.Topics.Add(topic);
            await _context.SaveChangesAsync();
            TopicUser topicUser = new TopicUser()
            {
                TopicId = topic.TopicId,
                UserId = userId,
            };
            _context.TopicUsers.Add(topicUser);
            await _context.SaveChangesAsync();
            return topic;
        }

        public async Task<IEnumerable<Topic>> GetTopicByUserId(int userId)
        {
            var userIds = await _context.TopicUsers.Where(u => u.UserId == userId).Select(x => x.TopicId).ToListAsync();
            var topics = await _context.Topics.Where(x => userIds.Contains(x.TopicId) && x.UserId != userId).ToListAsync();
            return topics;
        }

        public bool TopicExist(int topicId)
        {
            return _context.Topics.Any(x => x.TopicId == topicId);
        }

        public async Task JoinTopic(TopicUser tu)
        {
            await _context.TopicUsers.AddAsync(tu);
            await _context.SaveChangesAsync();
        }

        public bool UserExistsInTopic(int userId, int topicId)
        {
            return _context.TopicUsers.Any(x => x.UserId == userId && x.TopicId == topicId);
        }
        public async Task<IEnumerable<Topic>> GetCreatedTopics(int userId)
        {
            var topics = await _context.Topics.Where(x => x.UserId == userId).ToListAsync();
            return topics;
        }

        public async Task<IEnumerable<User>> GetAllTopicUsers(int topicId)
        {
            var userIds = await _context.TopicUsers.Where(x => x.TopicId == topicId).Select(x => x.UserId).ToListAsync();
            var users = await _context.Users.Where(x => userIds.Contains(x.UserId)).ToListAsync();
            return users;
        }

    }
}
