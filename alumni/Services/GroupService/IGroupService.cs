﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models.Domain;
using alumni.Models.Domain.EF_ManyToMany;
using alumni.Models.Dto.Invite;

namespace alumni.Services.GroupService
{
    public interface IGroupService
    {
        public Task<IEnumerable<Group>> GetAllGroups();
        public Task<IEnumerable<Group>> GetAllGroupsNotPrivate();
        public Task<Group> GetGroupById(int groupID);
        public Task<Group> CreateGroup(Group group, int userId);
        public Task<IEnumerable<User>> GetUserInGroup(int groupID);
        public Task<IEnumerable<Event>> GetEventInGroup(int groupId);
        public Task<IEnumerable<Group>> GetAllGroupsInAEvent(int eventID);
        public Task<IEnumerable<Group>> GetAllGroupsByUserId(int userID);
        public Task<GroupUser> JoinGroup(GroupUser groupUser);
        public bool UserExistsInGroup(int userId, int groupId);
        public Task<IEnumerable<Group>> GetCreatedGroups(int userId);
        public Task Invite(Invite invite);
        public bool InviteExist(int inviteId);
        public Task<Invite> GetInviteById(int inviteId);
        public Task UpdateInvite(Invite invite);
        public Task<IEnumerable<Group>> GetGroupsByGroupIds(List<int> groupIds);
        public Task<User> GetUserByUserId(int userId);
        public Task<IEnumerable<Invite>> CheckInvites(int userId);
        public bool InviteAlreadyExists(InviteCreateDTO inviteCreateDTO);
    }
}
