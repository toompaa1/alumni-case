﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models;
using alumni.Models.Domain;
using alumni.Models.Domain.EF_ManyToMany;
using alumni.Models.Dto.Invite;
using alumni.Models.Enums;
using Microsoft.EntityFrameworkCore;


namespace alumni.Services.GroupService
{
    public class GroupService : IGroupService
    {

        private readonly AlumniDbContext _context;
        public GroupService(AlumniDbContext context)
        {
            _context = context;
        }

        public async Task<Group> CreateGroup(Group group, int userId)
        {
            _context.Groups.Add(group);
            await _context.SaveChangesAsync();
            GroupUser gp = new GroupUser()
            {
                GroupId = group.GroupId,
                UserId = userId
            };
            _context.GroupUsers.Add(gp);
            await _context.SaveChangesAsync();

            return group;
        }

        public async Task<IEnumerable<Group>> GetAllGroups()
        {
            return await _context.Groups.Include(g => g.GroupUsers).ToListAsync();

        }
        // ta ut alla grupper som är inbjuda till ett event
        public async Task<IEnumerable<Group>> GetAllGroupsInAEvent(int eventId)
        {
            var GroupsIds = await _context.EventGroups.Where(u => u.EventId == eventId).Select(x => x.GroupId).ToListAsync();
            var Groups = await _context.Groups.Where(x => GroupsIds.Contains(x.GroupId)).ToListAsync();
            return Groups;
        }

        public async Task<IEnumerable<Group>> GetAllGroupsNotPrivate()
        {
            return await _context.Groups.Include(g => g.GroupUsers).Where(g => g.IsPrivate == false).ToListAsync();

        }

        public async Task<IEnumerable<Event>> GetEventInGroup(int groupId)
        {
            var eventIds = await _context.EventGroups.Where(x => x.GroupId == groupId).Select(x => x.EventId).ToListAsync();
            var events = await _context.Events.Where(x => eventIds.Contains(x.EventId)).ToListAsync();
            return events;
        }

        public async Task<Group> GetGroupById(int groupID)
        {
            return await _context.Groups.FindAsync(groupID);
        }

        public async Task<IEnumerable<User>> GetUserInGroup(int groupID)
        {

            var userIds = await _context.GroupUsers.Where(u => u.GroupId == groupID).Select(x => x.UserId).ToListAsync();
            var users = await _context.Users.Where(x => userIds.Contains(x.UserId)).ToListAsync();
            return users;


        }
        async Task<GroupUser> IGroupService.JoinGroup(GroupUser groupUser)
        {
            _context.GroupUsers.Add(groupUser);
            await _context.SaveChangesAsync();
            return groupUser;
        }
        public async Task<IEnumerable<Group>> GetAllGroupsByUserId(int userId)
        {
            var groupsIds = await _context.GroupUsers.Where(u => u.UserId == userId).Select(x => x.GroupId).ToListAsync();
            var groups = await _context.Groups.Where(x => groupsIds.Contains(x.GroupId) && x.UserId != userId).ToListAsync();
            return groups;
        }

        public bool UserExistsInGroup(int userId, int groupId)
        {
            return _context.GroupUsers.Any(x => x.UserId == userId && x.GroupId == groupId);
        }

        public async Task<IEnumerable<Group>> GetCreatedGroups(int userId)
        {
            var groups = await _context.Groups.Where(x => x.UserId == userId).ToListAsync();
            return groups;
        }

        public async Task Invite(Invite invite)
        {
            await _context.Invites.AddAsync(invite);
            await _context.SaveChangesAsync();
        }

        public async Task<Invite> GetInviteById(int inviteId)
        {
            var invite = await _context.Invites.Where(x => x.InviteId == inviteId).FirstAsync();
            return invite;
        }
        public async Task UpdateInvite(Invite invite)
        {
            _context.Entry(invite).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool InviteExist(int inviteId)
        {
            return _context.Invites.Any(x => x.InviteId == inviteId);
        }

        public async Task<IEnumerable<Group>> GetGroupsByGroupIds(List<int> groupIds)
        {
            var groups = await _context.Groups.Where(x => groupIds.Contains(x.GroupId)).ToListAsync();
            return groups;
        }

        public async Task<User> GetUserByUserId(int userId)
        {
            var user = await _context.Users.Where(x => x.UserId == userId).FirstAsync();
            return user;
        }

        public async Task<IEnumerable<Invite>> CheckInvites(int userId)
        {
            var Invites = await _context.Invites.Where(x => x.TargetUserId == userId && x.TargetType == TargetType.Group && x.Status == StatusType.Pending).ToListAsync();
            return Invites;
        }
        public bool InviteAlreadyExists(InviteCreateDTO inviteCreateDTO)
        {
            return _context.Invites.Any(x => x.TargetType == inviteCreateDTO.TargetType && x.TargetTypeId == inviteCreateDTO.TargetTypeId && x.TargetUserId == inviteCreateDTO.TargetUserId && (x.Status == StatusType.Pending || x.Status == StatusType.Accept));
        }
    }
}
