﻿using alumni.Models;
using alumni.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace alumni.Services.CommentService
{
    public class CommentService : ICommentService
    {
        private readonly AlumniDbContext _dbContext;
        public CommentService(AlumniDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool CommentExist(int id)
        {
            return _dbContext.Comments.Any(x => x.CommentId == id);
        }

        public async Task<Comment> CreateComment(Comment comment)
        {
            await _dbContext.Comments.AddAsync(comment);
            await _dbContext.SaveChangesAsync();
            return comment;
        }

        public async Task DeleteComment(Comment comment)
        {
            _dbContext.Comments.Remove(comment);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Comment>> GetAllCommentsByPostId(int id)
        {
            var comments = await _dbContext.Comments.Where(x => x.PostId == id).ToListAsync();
            return comments;
        }

        public async Task<Comment> GetCommentById(int id)
        {
            var comment = await _dbContext.Comments.Where(x => x.CommentId == id).AsNoTracking().FirstAsync();
            return comment;
        }

        public bool ParentPostExist(int id)
        {
            return _dbContext.Posts.Any(x => x.PostId == id);
        }

        public async Task UpdateComment(Comment comment)
        {
            _dbContext.Entry(comment).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }
    }
}
