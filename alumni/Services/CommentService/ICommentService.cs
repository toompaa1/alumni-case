﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models.Domain;

namespace alumni.Services.CommentService
{
    public interface ICommentService
    {
        public Task<Comment> GetCommentById(int id);

        public Task<IEnumerable<Comment>> GetAllCommentsByPostId(int id);

        public Task UpdateComment(Comment comment);

        public Task DeleteComment(Comment comment);

        public Task<Comment> CreateComment(Comment comment);

        public bool ParentPostExist(int id);

        public bool CommentExist(int id);
    }
}
