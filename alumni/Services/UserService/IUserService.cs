﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models.Domain;
namespace alumni.Services.UserService
{
    public interface IUserService
    {
        public Task<IEnumerable<User>> getUsers();
        public Task<IEnumerable<User>> GetAllUserComments();
        public Task<User> GetUserById(int userId);
        public Task UpdateUser(User user);
        public Task<User> CreateUser(User user);
        public bool Exist(string keycloakId);
        public Task<User> GetUserByKeycloakId(string keycloakId);
    }
}
