﻿using alumni.Models;
using alumni.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
namespace alumni.Services.UserService
{
    public class UserService : IUserService
    {
        private readonly AlumniDbContext _dbContext;
        public UserService(AlumniDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<User> CreateUser(User user)
        {
            _dbContext.Users.Add(user);
            await _dbContext.SaveChangesAsync();
            return user;
        }
        public async Task<User> GetUserByKeycloakId(string keycloakId)
        {
            var user = await _dbContext.Users.Where(x => x.KeycloakId == keycloakId).FirstAsync();
            return user;
        }
        public bool Exist(string keycloakId)
        {
            return _dbContext.Users.Any(x => x.KeycloakId == keycloakId);
        }

        public async Task<IEnumerable<User>> GetAllUserComments()
        {
            var userComments = await _dbContext.Users.Include(g => g.Comments).ToListAsync();
            return userComments;
        }

        public async Task<User> GetUserById(int userId)
        {
            return await _dbContext.Users.FindAsync(userId);
        }

        public async Task<IEnumerable<User>> getUsers()
        {
            return await _dbContext.Users.ToListAsync();
        }

        public async Task UpdateUser(User user)
        {
            _dbContext.Entry(user).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

        }
    }
}
