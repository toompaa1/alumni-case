﻿using alumni.Models;
using alumni.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using alumni.Models.Enums;
using alumni.Models.Dto.Post;

namespace alumni.Services.PostService
{
    public class PostService : IPostService
    {
        private readonly AlumniDbContext _dbContext;
        public PostService(AlumniDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool Exist(int id)
        {
            return _dbContext.Users.Any(x => x.UserId == id);
        }

        public async Task<IEnumerable<Post>> GetDirectMessages(int userId, int targetUserId)
        {
            var posts = await _dbContext.Posts.Where(x => x.CreatedByUserId == userId && x.TargetByUserId == targetUserId).ToListAsync();
            return posts;
        }

        public async Task<IEnumerable<PostReadDto>> GetPostsByEvent(int id)
        {
            var posts = from post in _dbContext.Posts
                        join u in _dbContext.Users on post.CreatedByUserId equals u.UserId
                        where id == post.EventId
                        select new
                        {
                            Title = post.Title,
                            PostId = post.PostId,
                            Date = post.Date,
                            LastUpdated = post.LastUpdated,
                            Description = post.Description,
                            FirstName = u.FirstName,
                            LastName = u.LastName
                        };
            var postList = await posts.ToListAsync();
            return postList.Select(p => new PostReadDto()
            {
                Title = p.Title,
                PostId = p.PostId,
                Date = p.Date,
                LastUpdated = p.LastUpdated,
                Description = p.Description,
                FirstName = p.FirstName,
                LastName = p.LastName
            }).ToList();
        }

        public async Task<IEnumerable<PostReadDto>> GetPostsByGoup(int id)
        {
            var posts = from post in _dbContext.Posts
                        join u in _dbContext.Users on post.CreatedByUserId equals u.UserId
                        where id == post.GroupId
                        select new
                        {
                            Title = post.Title,
                            PostId = post.PostId,
                            Date = post.Date,
                            LastUpdated = post.LastUpdated,
                            Description = post.Description,
                            FirstName = u.FirstName,
                            LastName = u.LastName
                        };
            var postList = await posts.ToListAsync();
            return postList.Select(p => new PostReadDto()
            {
                Title = p.Title,
                PostId = p.PostId,
                Date = p.Date,
                LastUpdated = p.LastUpdated,
                Description = p.Description,
                FirstName = p.FirstName,
                LastName = p.LastName
            }).ToList();
        }

        public async Task<IEnumerable<PostReadDto>> GetPostsByTopic(int id)
        {
            var posts = from post in _dbContext.Posts
                        join u in _dbContext.Users on post.CreatedByUserId equals u.UserId
                        where id == post.TopicId
                        select new
                        {
                            Title = post.Title,
                            PostId = post.PostId,
                            Date = post.Date,
                            LastUpdated = post.LastUpdated,
                            Description = post.Description,
                            FirstName = u.FirstName,
                            LastName = u.LastName
                        };
            var postList = await posts.ToListAsync();
            return postList.Select(p => new PostReadDto()
            {
                Title = p.Title,
                PostId = p.PostId,
                Date = p.Date,
                LastUpdated = p.LastUpdated,
                Description = p.Description,
                FirstName = p.FirstName,
                LastName = p.LastName
            }).ToList();
        }

        /// <summary>
        /// Getting topic and group posts related to a given user.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> A list of posts</returns>
        public async Task<IEnumerable<PostReadDto>> GetUserRelatedPosts(int id)
        {
            var eventIds = await _dbContext.EventUsers.Where(x => x.UserId == id).Select(x => x.EventId).ToListAsync();
            var groupIds = await _dbContext.GroupUsers.Where(x => x.UserId == id).Select(x => x.GroupId).ToListAsync();
            var topicIds = await _dbContext.TopicUsers.Where(x => x.UserId == id).Select(x => x.TopicId).ToListAsync();

            var posts = from post in _dbContext.Posts
                        join u in _dbContext.Users on post.CreatedByUserId equals u.UserId
                        select new
                        {
                            Title = post.Title,
                            PostId = post.PostId,
                            Date = post.Date,
                            LastUpdated = post.LastUpdated,
                            Description = post.Description,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            EventId = post.EventId,
                            GroupId = post.GroupId,
                            TopicId = post.TopicId
                        };
            var relatedPostList = await posts.Where(x => eventIds.Contains((int)x.EventId) || groupIds.Contains((int)x.GroupId) || topicIds.Contains((int)x.TopicId)).ToListAsync();
            return relatedPostList.Select(p => new PostReadDto()
            {
                Title = p.Title,
                PostId = p.PostId,
                Date = p.Date,
                LastUpdated = p.LastUpdated,
                Description = p.Description,
                FirstName = p.FirstName,
                LastName = p.LastName
            }).ToList();
        }

        public async Task<Post> CreatePost(Post post)
        {
            await _dbContext.Posts.AddAsync(post);
            await _dbContext.SaveChangesAsync();
            return post;
        }

        public async Task<PostReadDto> GetPostById(int postId)
        {
            var posts = from post in _dbContext.Posts
                        join u in _dbContext.Users on post.CreatedByUserId equals u.UserId
                        where postId == post.PostId
                        select new
                        {
                            Title = post.Title,
                            PostId = post.PostId,
                            Date = post.Date,
                            LastUpdated = post.LastUpdated,
                            Description = post.Description,
                            FirstName = u.FirstName,
                            LastName = u.LastName
                        };
            var postList = await posts.FirstAsync();
            return new PostReadDto()
            {
                Title = postList.Title,
                PostId = postList.PostId,
                Date = postList.Date,
                LastUpdated = postList.LastUpdated,
                Description = postList.Description,
                FirstName = postList.FirstName,
                LastName = postList.LastName
            };
        }

        public async Task UpdatePost(Post post)
        {
            _dbContext.Entry(post).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        public bool PostExist(int id)
        {
            return _dbContext.Posts.Any(x => x.PostId == id);
        }
    }
}
