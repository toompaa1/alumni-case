﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models.Domain;
using alumni.Models.Dto.Post;
using alumni.Models.Enums;

namespace alumni.Services.PostService
{
    public interface IPostService
    {
        public Task<IEnumerable<PostReadDto>> GetUserRelatedPosts(int id);
        public Task<IEnumerable<Post>> GetDirectMessages(int userId, int targetUserId);
        public Task<IEnumerable<PostReadDto>> GetPostsByGoup(int id);
        public Task<IEnumerable<PostReadDto>> GetPostsByTopic(int id);
        public Task<IEnumerable<PostReadDto>> GetPostsByEvent(int id);
        public Task<PostReadDto> GetPostById(int postId);
        public Task<Post> CreatePost(Post post);
        public Task UpdatePost(Post post);
        public bool PostExist(int id);
        public bool Exist(int id);
    }
}