﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models.Domain;
using alumni.Models.Domain.EF_ManyToMany;
using alumni.Models.Dto.Invite;

namespace alumni.Services.EventService
{
    public interface IEventService
    {
        public Task<IEnumerable<Event>> GetAllEvents();
        public Task<IEnumerable<Topic>> GetAllEventTopics(int eventId);
        public Task<IEnumerable<User>> GetAllEventUsers(int eventId);
        public Task<IEnumerable<Event>> GetAllEventsByUserId(int userId);
        public Task<Event> GetEventById(int eventId);
        public Task<Event> CreateEvent(Event _event, int userId);
        public Task UpdateEvent(Event _event);
        public bool EventExist(int eventId);
        public Task DeleteEvent(int eventId);
        public Task<IEnumerable<Event>> GetAllEventsByGroupId(int groupId);
        public Task JoinEvent(EventUser eventUser);
        public bool UserExistsInEvent(int userId, int eventId);
        public Task<IEnumerable<Event>> GetAllEventsByTopicId(int topicId);
        public Task Invite(Invite invite);
        public Task<IEnumerable<Event>> GetEventsByCreatedId(int userId);
        public Task<IEnumerable<Invite>> CheckInvites(int userId);
        public Task<IEnumerable<Event>> GetEventsByEventIds(List<int> eventIds);
        public Task<User> GetUserByUserId(int userId);
        public Task UpdateInvite(Invite invite);
        public Task<Invite> GetInviteById(int inviteId);
        public bool InviteExist(int inviteId);
        public bool InviteAlreadyExists(InviteCreateDTO inviteCreateDTO);
    }
}
