﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models;
using alumni.Models.Domain;
using alumni.Models.Domain.EF_ManyToMany;
using alumni.Models.Dto.Invite;
using alumni.Models.Enums;
using Microsoft.EntityFrameworkCore;

namespace alumni.Services.EventService
{
    public class EventService : IEventService
    {
        private readonly AlumniDbContext _context;
        public EventService(AlumniDbContext alumniDbContext)
        {
            _context = alumniDbContext;
        }

        public async Task<IEnumerable<Event>> GetAllEvents()
        {
            return await _context.Events.ToListAsync();
        }

        public async Task<IEnumerable<User>> GetAllEventUsers(int eventId)
        {
            var userIds = await _context.EventUsers.Where(u => u.EventId == eventId).Select(x => x.UserId).ToListAsync();
            var user = await _context.Users.Where(x => userIds.Contains(x.UserId)).ToListAsync();
            return user;
        }

        public async Task<IEnumerable<Topic>> GetAllEventTopics(int eventId)
        {
            var topicIds = await _context.EventTopics.Where(t => t.EventId == eventId).Select(t => t.TopicId).ToListAsync();
            var topic = await _context.Topics.Where(x => topicIds.Contains(x.TopicId)).ToListAsync();
            return topic;
        }

        public async Task<IEnumerable<Event>> GetAllEventsByGroupId(int groupId)
        {
            var eventIds = await _context.EventGroups.Where(u => u.GroupId == groupId).Select(x => x.EventId).ToListAsync();
            var events = await _context.Events.Where(x => eventIds.Contains(x.EventId)).ToListAsync();
            return events;
        }

        public async Task<IEnumerable<Event>> GetAllEventsByUserId(int userId)
        {
            var eventIds = await _context.EventUsers.Where(u => u.UserId == userId).Select(x => x.EventId).ToListAsync();
            var events = await _context.Events.Where(x => eventIds.Contains(x.EventId) && x.UserId != userId).ToListAsync();
            return events;
        }

        public async Task<IEnumerable<Event>> GetEventsByCreatedId(int userId)
        {
            var events = await _context.Events.Where(x => x.UserId == userId).ToListAsync();
            return events;
        }

        public bool UserExistsInEvent(int userId, int eventId)
        {
            return _context.EventUsers.Any(x => x.UserId == userId && x.EventId == eventId);
        }

        public async Task<Event> GetEventById(int eventId)
        {
            return await _context.Events.FindAsync(eventId);
        }

        public async Task<Event> CreateEvent(Event _event, int userId)
        {
            _context.Events.Add(_event);
            await _context.SaveChangesAsync();
            EventUser eu = new EventUser() { EventId = _event.EventId, UserId = userId };
            _context.EventUsers.Add(eu);
            await _context.SaveChangesAsync();
            return _event;
        }

        public async Task<IEnumerable<Event>> GetAllEventsByTopicId(int topicId)
        {
            var eventIds = await _context.EventTopics.Where(u => u.TopicId == topicId).Select(x => x.EventId).ToListAsync();
            var events = await _context.Events.Where(x => eventIds.Contains(x.EventId)).ToListAsync();
            return events;
        }

        public async Task UpdateEvent(Event _event)
        {
            _context.Entry(_event).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool EventExist(int eventId)
        {
            return _context.Events.Any(x => x.EventId == eventId);
        }

        public async Task DeleteEvent(int eventId)
        {
            var _event = await _context.Events.FindAsync(eventId);
            _context.Events.Remove(_event);
            await _context.SaveChangesAsync();
        }

        public async Task JoinEvent(EventUser eventUser)
        {
            await _context.EventUsers.AddAsync(eventUser);
            await _context.SaveChangesAsync();
        }

        public async Task Invite(Invite invite)
        {
            await _context.Invites.AddAsync(invite);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Invite>> CheckInvites(int userId)
        {
            var Invites = await _context.Invites.Where(x => x.TargetUserId == userId && x.TargetType == TargetType.Event && x.Status == StatusType.Pending).ToListAsync();
            return Invites;
        }

        public async Task<IEnumerable<Event>> GetEventsByEventIds(List<int> eventIds)
        {
            var events = await _context.Events.Where(x => eventIds.Contains(x.EventId)).ToListAsync();
            return events;
        }

        public bool InviteAlreadyExists(InviteCreateDTO inviteCreateDTO)
        {
            return _context.Invites.Any(x => x.TargetType == inviteCreateDTO.TargetType && x.TargetTypeId == inviteCreateDTO.TargetTypeId && x.TargetUserId == inviteCreateDTO.TargetUserId && (x.Status == StatusType.Pending || x.Status == StatusType.Accept));
        }

        public async Task<User> GetUserByUserId(int userId)
        {
            var user = await _context.Users.Where(x => x.UserId == userId).FirstAsync();
            return user;
        }

        public async Task UpdateInvite(Invite invite)
        {
            _context.Entry(invite).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<Invite> GetInviteById(int inviteId)
        {
            var invite = await _context.Invites.Where(x => x.InviteId == inviteId).FirstAsync();
            return invite;
        }

        public bool InviteExist(int inviteId)
        {
            return _context.Invites.Any(x => x.InviteId == inviteId);
        }
    }
}
