﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace alumni.Models.Domain
{
    public class Comment
    {
        public int CommentId { get; set; }
        public int? PostId { get; set; }
        public Post Post { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
        public string Text { get; set; }
        public string Date { get; set; }
        public bool Deleted { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
    }
}