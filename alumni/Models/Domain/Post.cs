﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace alumni.Models.Domain
{
    public class Post
    {
        public int PostId { get; set; }
        [MaxLength(100)]
        public string Title { get; set; }
        [MaxLength(2000)]
        public string Description { get; set; }
        [MaxLength(50)]
        public string Date { get; set; }
        [MaxLength(50)]
        public string LastUpdated { get; set; }
        public int? TopicId { get; set; }
        public Topic Topic { get; set; }
        public int? EventId { get; set; }
        public Event Event { get; set; }
        public int? GroupId { get; set; }
        public Group Group { get; set; }
        [ForeignKey("CreatedUser")]
        public int? CreatedByUserId { get; set; }
        public User CreatedUser { get; set; }
        [ForeignKey("TargetUser")]
        public int? TargetByUserId { get; set; }
        public User TargetUser { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public bool Deleted { get; set; }
    }
}