﻿using System;

namespace alumni.Models.Domain.EF_ManyToMany
{
    public class EventTopic
    {
        public int EventId { get; set; }
        public Event Event { get; set; }
        public int TopicId { get; set; }
        public Topic Topic { get; set; }
    }
}