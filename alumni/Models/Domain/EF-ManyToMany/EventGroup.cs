﻿using System;

namespace alumni.Models.Domain.EF_ManyToMany
{
    public class EventGroup
    {
        public int GroupId { get; set; }
        public Group Group { get; set; }
        public int EventId { get; set; }
        public Event Event { get; set; }
    }
}