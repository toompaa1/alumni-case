﻿using System;

namespace alumni.Models.Domain.EF_ManyToMany
{
    public class GroupUser
    {
        public int GroupId { get; set; }
        public Group Group { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}