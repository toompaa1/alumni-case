﻿using System;

namespace alumni.Models.Domain.EF_ManyToMany
{
    public class EventUser
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int EventId { get; set; }
        public Event Event { get; set; }
    }
}