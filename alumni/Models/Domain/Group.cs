﻿using alumni.Models.Domain.EF_ManyToMany;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using alumni.Models.Domain;

namespace alumni.Models.Domain
{
    public class Group
    {
        public int GroupId { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(2083)]
        public string Description { get; set; }
        public bool IsPrivate { get; set; }
        public bool Deleted { get; set; }
        public ICollection<GroupUser> GroupUsers { get; set; }
        public ICollection<EventGroup> EventGroups { get; set; }

    }
}