﻿using alumni.Models.Domain.EF_ManyToMany;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using alumni.Models.Domain;

namespace alumni.Models.Domain
{
    public class User
    {
        public int UserId { get; set; }

        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        [MaxLength(500)]
        public string Bio { get; set; }
        [MaxLength(1000)]
        public string FunFact { get; set; }
        [MaxLength(100)]
        public string WorkStatus { get; set; }
        [MaxLength(100)]
        public string KeycloakId { get; set; }
        public bool Deleted { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<GroupUser> GroupUsers { get; set; }
        public ICollection<EventUser> EventUsers { get; set; }
        public ICollection<Topic> Topics { get; set; }
        public ICollection<Event> Events { get; set; }
        public ICollection<Group> Groups { get; set; }
    }
}