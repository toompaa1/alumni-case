﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using alumni.Models.Enums;
namespace alumni.Models.Domain
{
    public class Invite
    {
        public int InviteId { get; set; }
        [ForeignKey("CreatedUser")]
        public int CreatedUserId { get; set; }
        public User CreatedUser { get; set; }
        [ForeignKey("TargetUser")]
        public int TargetUserId { get; set; }
        public User TargetUser { get; set; }
        public StatusType Status { get; set; }
        public TargetType TargetType { get; set; }
        public int TargetTypeId { get; set; }
    }
}
