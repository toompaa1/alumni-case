﻿using alumni.Models.Domain.EF_ManyToMany;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace alumni.Models.Domain
{
    public class Event
    {
        public int EventId { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(2083)]
        public string Description { get; set; }
        public bool IsPrivate { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool Deleted { get; set; }
        public ICollection<EventGroup> EventGroups { get; set; }
        public ICollection<EventTopic> EventTopics { get; set; }
        public ICollection<EventUser> EventUsers { get; set; }

    }
}