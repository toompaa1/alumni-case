﻿using alumni.Models.Domain.EF_ManyToMany;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace alumni.Models.Domain
{
    public class Topic
    {
        public int TopicId { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(2083)]
        public string Description { get; set; }
        public bool Deleted { get; set; }
        public ICollection<EventTopic> EventTopics { get; set; }
    }
}