﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Models.Enums
{
    public enum StatusType
    {
        Pending,
        Accept,
        Decline
    }
}
