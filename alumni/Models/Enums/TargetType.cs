﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Models.Enums
{
    public enum TargetType
    {
        Event,
        Group,
        Topic,
        User
    }
}
