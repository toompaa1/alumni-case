﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models.Domain;
using Microsoft.EntityFrameworkCore;
using alumni.Models.Domain.EF_ManyToMany;
using Microsoft.EntityFrameworkCore.Metadata;

namespace alumni.Models
{
    public class AlumniDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Invite> Invites { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<EventGroup> EventGroups { get; set; }
        public DbSet<EventTopic> EventTopics { get; set; }
        public DbSet<EventUser> EventUsers { get; set; }
        public DbSet<GroupUser> GroupUsers { get; set; }
        public DbSet<TopicUser> TopicUsers { get; set; }
        public AlumniDbContext(DbContextOptions options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            modelBuilder.Entity<EventGroup>().HasKey(eg => new { eg.EventId, eg.GroupId });
            modelBuilder.Entity<EventTopic>().HasKey(et => new { et.EventId, et.TopicId });
            modelBuilder.Entity<EventUser>().HasKey(eu => new { eu.EventId, eu.UserId });
            modelBuilder.Entity<GroupUser>().HasKey(gu => new { gu.GroupId, gu.UserId });
            modelBuilder.Entity<TopicUser>().HasKey(tu => new { tu.TopicId, tu.UserId });

            modelBuilder.Entity<Comment>().HasData(new Comment
            {
                CommentId = 1,
                PostId = 1,
                UserId = 1,
                Text = "his is a testcomment, of user 1 on users  1 post",
                Date = "2021-09-07",
                Deleted = false,
            });
            modelBuilder.Entity<Comment>().HasData(new Comment
            {
                CommentId = 2,
                PostId = 2,
                UserId = 1,
                Text = "testing commenting on same post",
                Date = "2021-09-07",
                Deleted = false,
            });
            modelBuilder.Entity<Comment>().HasData(new Comment
            {
                CommentId = 3,
                PostId = 1,
                UserId = 2,
                Text = "testing comment",
                Date = "2021-09-07",
                Deleted = false,
            });
            modelBuilder.Entity<Event>().HasData(new Event
            {
                EventId = 1,
                Name = "VolvoMeet",
                Description = "A fan meet for those who love Volvos",
                IsPrivate = false,
                Deleted = false,
                StartDate = "2021-02-05",
                EndDate = "2021-04-02",
                UserId = 3,
            });
            modelBuilder.Entity<Event>().HasData(new Event
            {
                EventId = 2,
                Name = "IlluminatiMeeting",
                Description = "A meeting for the wise&true member of Illuminati",
                IsPrivate = true,
                Deleted = false,
                StartDate = "2021-02-05",
                EndDate = "2021-04-02",
                UserId = 1,
            });
            modelBuilder.Entity<Event>().HasData(new Event
            {
                EventId = 3,
                Name = "MercMeet",
                Description = "A fan meet for those who rides cars with a star",
                IsPrivate = false,
                Deleted = false,
                StartDate = "2021-02-05",
                EndDate = "2021-04-02",
                UserId = 2,
            });
            modelBuilder.Entity<Group>().HasData(new Group
            {
                GroupId = 1,
                Name = "VolvoGroup",
                Description = "A group of volvo fans",
                IsPrivate = false,
                Deleted = false,
                UserId = 1,
            });
            modelBuilder.Entity<Group>().HasData(new Group
            {
                GroupId = 2,
                Name = "Illuminati",
                Description = "a Group for the wise&true member of Illuminati",
                IsPrivate = true,
                Deleted = false,
                UserId = 2,
            });
            modelBuilder.Entity<Group>().HasData(new Group
            {
                GroupId = 3,
                Name = "MercGroup",
                Description = "A group for people who rides cars with a star",
                IsPrivate = false,
                Deleted = false,
                UserId = 3,
            });
            modelBuilder.Entity<Post>().HasData(new Post
            {
                PostId = 1,
                Title = "Help, my volvo have too much horsepower!!",
                Description = "This is a post about very fast cars. Faster than my bike.",
                Date = "2021-05-01",
                LastUpdated = "2021-05-02",
                Deleted = false,
                TopicId = null,
                EventId = null,
                GroupId = 1,
                CreatedByUserId = 1,
                TargetByUserId = null
            });
            modelBuilder.Entity<Post>().HasData(new Post
            {
                PostId = 2,
                Title = "We need to do someting about these skilled programmers at experis",
                Description = "Today we will talk about Angular and how freaking bad it is.",
                Date = "2021-09-01",
                LastUpdated = "2021-09-01",
                Deleted = false,
                TopicId = null,
                EventId = 2,
                GroupId = null,
                CreatedByUserId = 2,
                TargetByUserId = null
            });
            modelBuilder.Entity<Post>().HasData(new Post
            {
                PostId = 3,
                Title = "Help!! My merc wont start.....",
                Description = "Rijad started this post",
                Date = "2021-09-01",
                LastUpdated = "2021-09-01",
                Deleted = false,
                TopicId = null,
                EventId = null,
                GroupId = null,
                CreatedByUserId = 3,
                TargetByUserId = 1
            });
            modelBuilder.Entity<Topic>().HasData(new Topic
            {
                TopicId = 1,
                Name = "Cars",
                Description = "Topic about cars",
                Deleted = false,
                UserId = 1,
            });
            modelBuilder.Entity<Topic>().HasData(new Topic
            {
                TopicId = 2,
                Name = "Programming",
                Description = "Topic about programming",
                Deleted = false,
                UserId = 2,
            });
            modelBuilder.Entity<Topic>().HasData(new Topic
            {
                TopicId = 3,
                Name = "Banking and stocks",
                Description = "Topic about banking and stocks",
                Deleted = false,
                UserId = 3,
            });
            modelBuilder.Entity<User>().HasData(new User
            {
                UserId = 1,
                FirstName = "Viktor",
                LastName = "Bolinder",
                ProfilePicture = "URL...",
                Bio = "likes volvo and programming",
                FunFact = "Drives a 960 that is faster than most germans cars ",
                WorkStatus = "Woriking",
                Deleted = false,
                KeycloakId = "60927cc1-f16d-4e03-ae30-75588aec06b8"
            });
            modelBuilder.Entity<User>().HasData(new User
            {
                UserId = 2,
                FirstName = "Tommy",
                LastName = "Nguyen",
                ProfilePicture = "URL...",
                Bio = "Likes mercs and programming",
                FunFact = "drives a merc that is not faster than viktors 960",
                WorkStatus = "Woriking",
                Deleted = false,
                KeycloakId = "c2284ff3-1ffa-4c55-b77c-87130da296d3"
            });
            modelBuilder.Entity<User>().HasData(new User
            {
                UserId = 3,
                FirstName = "Måns",
                LastName = "Regin",
                ProfilePicture = "URL...",
                Bio = "Likes padel and programming",
                FunFact = "is kingkong, drives volvo",
                WorkStatus = "Woriking",
                Deleted = false,
                KeycloakId = "2ee1c775-af8a-4369-a405-60990bfcc103"
            });
            modelBuilder.Entity<User>().HasData(new User
            {
                UserId = 4,
                FirstName = "Rijad",
                LastName = "Hamula",
                ProfilePicture = "URL...",
                Bio = "Likes rakija",
                FunFact = "is kingkong, drives volvo",
                WorkStatus = "Drive slowest car ever",
                Deleted = false,
                KeycloakId = "19dc36d2-9023-45b9-8ce6-241428d49c3e"
            });
            modelBuilder.Entity<EventGroup>().HasData(new EventGroup
            {
                GroupId = 1,
                EventId = 1,
            });
            modelBuilder.Entity<EventGroup>().HasData(new EventGroup
            {
                GroupId = 2,
                EventId = 2,
            });
            modelBuilder.Entity<EventGroup>().HasData(new EventGroup
            {
                GroupId = 3,
                EventId = 3,
            });
            modelBuilder.Entity<EventTopic>().HasData(new EventTopic
            {
                TopicId = 1,
                EventId = 1,
            });
            modelBuilder.Entity<EventUser>().HasData(new EventUser
            {
                UserId = 1,
                EventId = 1,
            });
            modelBuilder.Entity<EventUser>().HasData(new EventUser
            {
                UserId = 1,
                EventId = 2,
            });
            modelBuilder.Entity<EventUser>().HasData(new EventUser
            {
                UserId = 1,
                EventId = 3,
            });
            modelBuilder.Entity<EventUser>().HasData(new EventUser
            {
                UserId = 2,
                EventId = 1,
            });
            modelBuilder.Entity<EventUser>().HasData(new EventUser
            {
                UserId = 4,
                EventId = 2,
            });
            modelBuilder.Entity<EventUser>().HasData(new EventUser
            {
                UserId = 3,
                EventId = 1,
            });
            modelBuilder.Entity<GroupUser>().HasData(new GroupUser
            {
                GroupId = 1,
                UserId = 1,
            });
            modelBuilder.Entity<GroupUser>().HasData(new GroupUser
            {
                GroupId = 1,
                UserId = 2,
            });
            modelBuilder.Entity<GroupUser>().HasData(new GroupUser
            {
                GroupId = 3,
                UserId = 4,
            });
            modelBuilder.Entity<GroupUser>().HasData(new GroupUser
            {
                GroupId = 2,
                UserId = 2,
            });
            modelBuilder.Entity<TopicUser>().HasData(new TopicUser
            {
                TopicId = 1,
                UserId = 2,
            });
        }
    }
}
