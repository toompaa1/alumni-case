﻿using alumni.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Models.Dto.Comment
{
    public class CommentReadDTO
    {
        public int CommentId { get; set; }
        public string Text { get; set; }
        public string Date { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
