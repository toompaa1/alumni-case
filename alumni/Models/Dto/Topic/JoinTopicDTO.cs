﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Models.Dto.Topic
{
    public class JoinTopicDTO
    {
        public int TopicId { get; set; }
        public int UserId { get; set; }
    }
}
