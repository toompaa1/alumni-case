﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace alumni.Models.Dto.Topic
{
    public class TopicCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
