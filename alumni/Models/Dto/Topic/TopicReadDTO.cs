﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Models.Dto.Topic
{
    public class TopicReadDTO
    {
        public int TopicId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int> Events { get; set; }
    }
}
