﻿using alumni.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Models.Dto.Post
{
    public class PostCreateDTO
    {

        public string Title { get; set; }
        public string Date { get; set; }
        public string LastUpdated { get; set; }
        public TargetType TargetType { get; set; }
        public int CreatedByUserId { get; set; }
        public int TargetId { get; set; }
        public string Description { get; set; }
    }
}
