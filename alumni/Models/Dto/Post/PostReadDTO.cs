﻿using alumni.Models.Domain;
using alumni.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Models.Dto.Post
{
    public class PostReadDto
    {
        public string Title { get; set; }
        public int PostId { get; set; }
        public string Date { get; set; }
        public string LastUpdated { get; set; }
        public string Description { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
    }
}
