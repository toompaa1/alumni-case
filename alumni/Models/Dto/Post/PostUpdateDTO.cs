﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Models.Dto.Post
{
    public class PostUpdateDTO
    {
        public string Title { get; set; }
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
