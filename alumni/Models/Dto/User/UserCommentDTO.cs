﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Models.Domain;

namespace alumni.Models.Dto.User
{
    public class UserCommentDTO
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        public string Bio { get; set; }
        public string FunFact { get; set; }
        public string WorkStatus { get; set; }
        public List<int> Comments { get; set; }

    }
}
