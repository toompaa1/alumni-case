﻿using alumni.Models.Domain.EF_ManyToMany;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Models.Dto.Group
{
    public class GroupUserDTO
    {
        public List<int> Users { get; set; }
    }
}
