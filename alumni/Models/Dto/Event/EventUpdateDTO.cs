﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace alumni.Models.Dto.Event
{
    public class EventUpdateDTO
    {
        public int EventId { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(2083)]
        public string Description { get; set; }
        public bool IsPrivate { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
