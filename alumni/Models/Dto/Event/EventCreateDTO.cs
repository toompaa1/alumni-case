﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace alumni.Models.Dto.Event
{
    public class EventCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsPrivate { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
