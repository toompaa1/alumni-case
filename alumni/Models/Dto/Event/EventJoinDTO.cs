﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Models.Dto.Event
{
    public class EventJoinDTO
    {
        public int EventId { get; set; }
        public int UserId { get; set; }
    }
}
