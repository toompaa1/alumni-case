﻿using alumni.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Models.Dto.Invite
{
    public class InviteCreateDTO
    {
        public int CreatedUserId { get; set; }
        public int TargetUserId { get; set; }
        public StatusType Status { get; set; }
        public TargetType TargetType { get; set; }
        public int TargetTypeId { get; set; }
    }
}
