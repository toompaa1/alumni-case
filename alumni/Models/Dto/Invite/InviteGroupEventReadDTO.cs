﻿using alumni.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Models.Dto.Invite
{
    public class InviteGroupEventReadDTO
    {
        public int InviteId { get; set; }
        public string InvitedUserName { get; set; }
        public string Name { get; set; }
        public int TargetTypeId { get; set; }
        public TargetType TargetType { get; set; }
    }
}
