﻿using alumni.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumni.Models.Dto.Invite
{
    public class InviteRespondDTO
    {
        public int InviteId { get; set; }
        public int TargetTypeId { get; set; }
        public StatusType Status { get; set; }
    }
}
