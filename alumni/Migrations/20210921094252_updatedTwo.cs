﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace alumni.Migrations
{
    public partial class updatedTwo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "EventUsers",
                columns: new[] { "EventId", "UserId" },
                values: new object[] { 1, 3 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "EventUsers",
                keyColumns: new[] { "EventId", "UserId" },
                keyValues: new object[] { 1, 3 });
        }
    }
}
