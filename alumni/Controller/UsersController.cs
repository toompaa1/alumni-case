﻿using alumni.Models.Domain;
using alumni.Models.Dto.User;
using alumni.Services.UserService;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace alumni.Controller
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UsersController(IMapper mapper, IUserService userService)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserCommentDTO>>> GetallUserComments()
        {
            return _mapper.Map<List<UserCommentDTO>>(await _userService.GetAllUserComments());
        }

        [HttpPost]
        public async Task<ActionResult<UserCreateDTO>> CreateUser(UserCreateDTO userDto)
        {
            User user = _mapper.Map<User>(userDto);
            user = await _userService.CreateUser(user);
            return CreatedAtAction("GetUserById", new { userId = user.UserId }, _mapper.Map<UserReadDTO>(user));

        }

        [HttpGet("keycloak/{keycloakId}")]
        public async Task<ActionResult<UserReadDTO>> GetUserByKeycloakId(string keycloakId)
        {
            if (!_userService.Exist(keycloakId))
            {
                return NotFound();
            }
            var user = await _userService.GetUserByKeycloakId(keycloakId);
            return _mapper.Map<UserReadDTO>(user);
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<UserReadDTO>> GetUserById(int userId)
        {
            User user = await _userService.GetUserById(userId);
            if (user == null)
            {
                return NotFound();
            }
            return _mapper.Map<UserReadDTO>(user);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<UserUpdateDTO>> UpdateUser(int id, UserUpdateDTO userUpdateDTO)
        {
            if (id != userUpdateDTO.UserId)
            {
                return BadRequest("Not working");
            }
            User user = _mapper.Map<User>(userUpdateDTO);
            user.UserId = userUpdateDTO.UserId;
            try
            {
                await _userService.UpdateUser(user);
            }
            catch
            {
                return BadRequest("Database Error");
            }
            return NoContent();
        }
    }
}
