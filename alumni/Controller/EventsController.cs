﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Services.EventService;
using alumni.Services.GroupService;
using AutoMapper;
using alumni.Models.Dto.Event;
using alumni.Models.Domain;
using Microsoft.AspNetCore.Cors;
using alumni.Models.Dto.Group;
using alumni.Models.Dto.User;
using alumni.Models.Dto.Topic;
using alumni.Models.Domain.EF_ManyToMany;
using alumni.Models.Dto.Invite;
using alumni.Models.Enums;
using Microsoft.AspNetCore.Authorization;

namespace alumni.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EventsController : ControllerBase
    {
        private readonly IEventService _eventService;
        private readonly IMapper _mapper;
        private readonly IGroupService _groupService;

        public EventsController(IEventService eventService, IMapper mapper, IGroupService groupService)
        {
            _eventService = eventService;
            _mapper = mapper;
            _groupService = groupService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<EventReadDTO>>> GetAllEvents()
        {
            return _mapper.Map<List<EventReadDTO>>(await _eventService.GetAllEvents());
        }

        [HttpGet("{userId}/event")]
        public async Task<ActionResult<IEnumerable<EventReadDTO>>> GetAllEventsByUserId(int userId)
        {
            var _event = await _eventService.GetAllEventsByUserId(userId);
            if (_event == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<EventReadDTO>>(_event);
        }

        [HttpGet("{eventId}/groups")]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetAllGroupsInAEvent(int eventId)
        {
            var _group = await _groupService.GetAllGroupsInAEvent(eventId);
            if (_group == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<GroupReadDTO>>(_group);
        }

        [HttpPost("join")]
        public async Task<ActionResult> JoinEvent(EventJoinDTO eventJoinDTO)
        {
            EventUser eventUser = _mapper.Map<EventUser>(eventJoinDTO);
            if (!_eventService.EventExist(eventJoinDTO.EventId))
            {
                return NotFound("Event does not exist.");
            }
            if (_eventService.UserExistsInEvent(eventJoinDTO.UserId, eventJoinDTO.EventId))
            {
                return BadRequest();
            }
            try
            {
                await _eventService.JoinEvent(eventUser);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok("Added to Event.");

        }


        [HttpGet("{groupId}/eventsInGroups")]
        public async Task<ActionResult<IEnumerable<EventReadDTO>>> GetAllEventsByGroupId(int groupId)
        {
            var _event = await _eventService.GetAllEventsByGroupId(groupId);
            if (_event == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<EventReadDTO>>(_event);
        }


        [HttpGet("{eventId}/users")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetAllEventUsers(int eventId)
        {
            var _eventUsers = await _eventService.GetAllEventUsers(eventId);
            if (_eventUsers == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<UserReadDTO>>(_eventUsers);
        }

        [HttpGet("{eventId}/topics")]
        public async Task<ActionResult<IEnumerable<TopicReadDTO>>> GetAllEventTopics(int eventId)
        {
            var _eventTopics = await _eventService.GetAllEventTopics(eventId);
            if (_eventTopics == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<TopicReadDTO>>(_eventTopics);
        }

        [HttpGet("{eventId}")]
        public async Task<ActionResult<EventReadDTO>> GetEventById(int eventId)
        {
            Event _event = await _eventService.GetEventById(eventId);
            if (_event == null)
            {
                return NotFound();
            }
            return _mapper.Map<EventReadDTO>(_event);
        }

        [HttpPost("user/{userId}")]
        public async Task<ActionResult<EventReadDTO>> CreateEvent(EventCreateDTO dtoEvent, int userId)
        {
            Event _event = _mapper.Map<Event>(dtoEvent);
            _event.UserId = userId;
            _event = await _eventService.CreateEvent(_event, userId);
            return CreatedAtAction("GetEventById", new { eventId = _event.EventId }, _mapper.Map<EventReadDTO>(_event));
        }

        [HttpGet("createdBy/{userId}")]
        public async Task<ActionResult<IEnumerable<EventReadDTO>>> GetEventsByCreatedUserId(int userId)
        {
            var events = await _eventService.GetEventsByCreatedId(userId);
            return _mapper.Map<List<EventReadDTO>>(events);
        }

        [HttpPut("{eventId}")]
        public async Task<ActionResult<EventUpdateDTO>> UpdateEvent(int eventId, EventUpdateDTO eventDto)
        {
            if (eventId != eventDto.EventId)
            {
                return BadRequest("Id is not matching");
            }
            if (!_eventService.EventExist(eventId))
            {
                return BadRequest("No existing post");
            }

            Event _event = _mapper.Map<Event>(eventDto);
            _event.EventId = eventDto.EventId;

            try
            {
                await _eventService.UpdateEvent(_event);
            }
            catch
            {
                return BadRequest("Database error");
            }

            return NoContent();
        }

        [HttpDelete("{eventId}")]
        public async Task<ActionResult> DeleteEvent(int eventId)
        {
            if (!_eventService.EventExist(eventId))
            {
                return NotFound();
            }
            await _eventService.DeleteEvent(eventId);
            return Ok();
        }

        [HttpGet("{topicId}/eventsInTopic")]
        public async Task<ActionResult<IEnumerable<EventReadDTO>>> GetAllEventsByTopicId(int topicId)
        {
            var _event = await _eventService.GetAllEventsByTopicId(topicId);
            if (_event == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<EventReadDTO>>(_event);
        }

        [HttpPost("invite/")]
        public async Task<ActionResult> InviteToEvent(InviteCreateDTO inviteCreateDTO)
        {
            Invite invite = _mapper.Map<Invite>(inviteCreateDTO);
            if (inviteCreateDTO.CreatedUserId == inviteCreateDTO.TargetUserId)
            {
                return BadRequest("Can not invite yourself");
            }
            if (_eventService.InviteAlreadyExists(inviteCreateDTO))
            {
                return BadRequest("This invite is pending or the user has already joined the event.");
            }
            try
            {
                await _eventService.Invite(invite);
            }
            catch (Exception)
            {
                return BadRequest("Not valid invite");
            }
            return Ok("Invited successfully");
        }

        [HttpGet("checkInviteRequest/{userId}")]
        public async Task<ActionResult<IEnumerable<InviteGroupEventReadDTO>>> CheckInvites(int userId)
        {
            try
            {
                var invites = await _eventService.CheckInvites(userId);
                //hämta med target tpye och target id
                var eventIds = invites.Where(x => x.TargetType == TargetType.Event).Select(x => x.TargetTypeId).ToList();
                IEnumerable<Event> eventList = new List<Event>();
                if (eventIds.Count > 0)
                {
                    eventList = await _eventService.GetEventsByEventIds(eventIds);
                }
                List<InviteGroupEventReadDTO> invList = new List<InviteGroupEventReadDTO>();
                if (eventIds.Count > 0)
                {
                    var userIds = invites.Where(x => x.TargetUserId == userId).Select(x => x.CreatedUserId).ToList();
                    var users = new List<User>();

                    foreach (int i in userIds)
                    {
                        users.Add(await _eventService.GetUserByUserId(i));
                    }

                    for (int i = 0; i < eventIds.Count; i++)
                    {
                        if (i < eventIds.Count)
                        {
                            InviteGroupEventReadDTO igerDTO = new InviteGroupEventReadDTO()
                            {
                                InviteId = invites.ElementAt(i).InviteId,
                                InvitedUserName = $"{users.ElementAt(i).FirstName} {users.ElementAt(i).LastName}",
                                Name = eventList.ElementAt(i).Name,
                                TargetTypeId = eventList.ElementAt(i).EventId,
                                TargetType = TargetType.Event,
                            };
                            invList.Add(igerDTO);
                        }
                    }
                }
                //return a list of new objects with invitereaddto and title and targetId
                return invList;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("respondToInvite/{inviteId}")]
        public async Task<ActionResult> RespondToInvite(int inviteId, InviteRespondDTO inviteRespondDTO)
        {
            if (inviteId != inviteRespondDTO.InviteId)
            {
                return BadRequest("Not correct ids");
            }
            if (!_eventService.InviteExist(inviteRespondDTO.InviteId))
            {
                return BadRequest("Invite does not exsit");
            }

            var invite = await _eventService.GetInviteById(inviteRespondDTO.InviteId);
            invite.Status = inviteRespondDTO.Status;
            await _eventService.UpdateInvite(invite);

            if (inviteRespondDTO.Status == StatusType.Accept)
            {
                EventUser eventUser = new EventUser()
                {
                    EventId = inviteRespondDTO.TargetTypeId,
                    UserId = invite.TargetUserId
                };
                if (_eventService.UserExistsInEvent(eventUser.UserId, eventUser.EventId))
                {
                    return NoContent();
                }
                try
                {
                    await _eventService.JoinEvent(eventUser);
                }
                catch (Exception)
                {

                    return BadRequest();
                }
                return Ok("Added to event");
            }
            else
            {
                return Ok("Declined invite");
            }

        }

    }
}
