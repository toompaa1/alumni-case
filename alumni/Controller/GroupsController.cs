﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Services.GroupService;
using AutoMapper;
using alumni.Models.Dto.Group;
using alumni.Models.Domain;
using alumni.Models.Domain.EF_ManyToMany;
using Microsoft.AspNetCore.Cors;
using alumni.Models.Dto.User;
using alumni.Models.Enums;
using alumni.Models.Dto.Invite;
using alumni.Models.Dto.Event;
using Microsoft.AspNetCore.Authorization;

namespace alumni.Controller
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GroupController : ControllerBase
    {
        private readonly IGroupService _GroupService;
        private readonly IMapper _mapper;

        public GroupController(IGroupService groupService, IMapper mapper)
        {
            _GroupService = groupService;
            _mapper = mapper;
        }
        /// <summary>
        /// Get all groups in database
        /// </summary>
        /// <returns> A list of the groups </returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetAllGroups()
        {
            return _mapper.Map<List<GroupReadDTO>>(await _GroupService.GetAllGroups());
        }

        [HttpGet("/GroupsThatAreNotPrivate")]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetAllGroupsNotPrivate()
        {
            List<GroupReadDTO> list = _mapper.Map<List<GroupReadDTO>>(await _GroupService.GetAllGroupsNotPrivate());

            if (list == null)
            {
                return NotFound();
            }
            return list;
        }

        /// <summary>
        /// Get a group by their ID
        /// </summary>
        /// <param name="groupId"> ID of the group you want to find </param>
        /// <returns>The Group</returns>
        [HttpGet("{groupId}")]
        public async Task<ActionResult<GroupReadDTO>> GetGroupById(int groupId)
        {
            Group group = await _GroupService.GetGroupById(groupId);
            if (group == null)
            {
                return NotFound();
            }
            return _mapper.Map<GroupReadDTO>(group);
        }

        /// <summary>
        /// Get all user that are member in a group
        /// </summary>
        /// <param name="groupId"> </param>
        /// <returns> A list with membersID </returns>
        [HttpGet("{groupId}/users")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUserInGroup(int groupId)
        {
            var users = await _GroupService.GetUserInGroup(groupId);
            if (users == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<UserReadDTO>>(users);
        }

        [HttpGet("{eventId}/GroupsInEvents")]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetAllGroupsInAEvent(int eventId)
        {
            var _gorup = await _GroupService.GetAllGroupsInAEvent(eventId);
            if (_gorup == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<GroupReadDTO>>(_gorup);
        }

        [HttpGet("{groupId}/events")]
        public async Task<ActionResult<IEnumerable<EventReadDTO>>> GetEventInGroup(int groupId)
        {
            var groupEvents = await _GroupService.GetEventInGroup(groupId);
            if (groupEvents == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<EventReadDTO>>(groupEvents);
        }

        /// <summary>
        /// Creat a new Group
        /// </summary>
        /// <param name="dtoGroup"></param>
        /// <returns>The new group</returns>
        [HttpPost("user/{userId}")]
        public async Task<ActionResult<GroupReadDTO>> CreateGroup(GroupCreateDTO dtoGroup, int userId)
        {
            Group group = _mapper.Map<Group>(dtoGroup);
            group.UserId = userId;
            group = await _GroupService.CreateGroup(group, userId);
            return CreatedAtAction("GetGroupById", new { groupId = group.GroupId }, _mapper.Map<GroupReadDTO>(group));
        }

        [HttpGet("{userId}/AllRelevantGroups")]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetAllGroupsByUserId(int userId)
        {
            var _group = await _GroupService.GetAllGroupsByUserId(userId);
            if (_group == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<GroupReadDTO>>(_group);
        }

        [HttpPost("Join")]
        public async Task<ActionResult<GroupJoinDTO>> JoinGroup(GroupJoinDTO groupJoinDTO)
        {
            if (_GroupService.UserExistsInGroup(groupJoinDTO.UserId, groupJoinDTO.GroupId))
            {
                return BadRequest("Already a member");
            }

            try
            {
                GroupUser groupUser = _mapper.Map<GroupUser>(groupJoinDTO);
                groupUser = await _GroupService.JoinGroup(groupUser);
            }
            catch
            {
                return BadRequest("The user is already a member");
            }
            return Ok("Added to group");
        }

        [HttpGet("{userId}/GetCreatedGroupsByUserId")]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetCreatedGroups(int userId)
        {
            var group = await _GroupService.GetCreatedGroups(userId);
            if (group == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<GroupReadDTO>>(group);
        }

        [HttpPut("respondToInvite/{inviteId}")]
        public async Task<ActionResult> RespondToInvite(int inviteId, InviteRespondDTO inviteRespondDTO)
        {
            if (inviteId != inviteRespondDTO.InviteId)
            {
                return BadRequest("Not correct ids");
            }
            if (!_GroupService.InviteExist(inviteRespondDTO.InviteId))
            {
                return BadRequest("Invite does not exsit");
            }
            var invite = await _GroupService.GetInviteById(inviteRespondDTO.InviteId);
            invite.Status = inviteRespondDTO.Status;
            await _GroupService.UpdateInvite(invite);
            if (inviteRespondDTO.Status == StatusType.Accept)
            {
                GroupUser groupUser = new GroupUser()
                {
                    GroupId = inviteRespondDTO.TargetTypeId,
                    UserId = invite.TargetUserId
                };
                if (_GroupService.UserExistsInGroup(groupUser.UserId, groupUser.GroupId))
                {
                    return NoContent();
                }
                try
                {
                    await _GroupService.JoinGroup(groupUser);
                }
                catch (Exception)
                {
                    return BadRequest();
                }
                return Ok("Added to event");
            }
            else
            {
                return Ok("Declined invite");
            }
        }

        [HttpPost("invite/")]
        public async Task<ActionResult> InviteToGroup(InviteCreateDTO inviteCreateDTO)
        {
            Invite invite = _mapper.Map<Invite>(inviteCreateDTO);
            if (inviteCreateDTO.CreatedUserId == inviteCreateDTO.TargetUserId)
            {
                return BadRequest("Can not invite yourself");
            }
            if (_GroupService.InviteAlreadyExists(inviteCreateDTO))
            {
                return BadRequest("This invite is pending or the user has already joined the group.");
            }
            try
            {
                await _GroupService.Invite(invite);
            }
            catch (Exception)
            {
                return BadRequest("Not valid invite");
            }
            return Ok("Invited successfully");
        }

        [HttpGet("checkInviteRequest/{userId}")]
        public async Task<ActionResult<IEnumerable<InviteGroupEventReadDTO>>> CheckInvites(int userId)
        {
            try
            {
                var invites = await _GroupService.CheckInvites(userId);
                var groupIds = invites.Where(x => x.TargetType == TargetType.Group).Select(x => x.TargetTypeId).ToList();
                IEnumerable<Group> groupList = new List<Group>();

                if (groupIds.Count > 0)
                {
                    groupList = await _GroupService.GetGroupsByGroupIds(groupIds);
                }

                List<InviteGroupEventReadDTO> invList = new List<InviteGroupEventReadDTO>();
                
                if (groupIds.Count > 0)
                {
                    var userIds = invites.Where(x => x.TargetUserId == userId).Select(x => x.CreatedUserId).ToList();
                    var users = new List<User>();
                    foreach (int i in userIds)
                    {
                        users.Add(await _GroupService.GetUserByUserId(i));
                    }

                    for (int i = 0; i < groupIds.Count; i++)
                    {
                        if (i < groupIds.Count)
                        {
                            InviteGroupEventReadDTO igerDTO = new InviteGroupEventReadDTO()
                            {
                                InviteId = invites.ElementAt(i).InviteId,
                                InvitedUserName = $"{users.ElementAt(i).FirstName} {users.ElementAt(i).LastName}",
                                Name = groupList.ElementAt(i).Name,
                                TargetTypeId = groupList.ElementAt(i).GroupId,
                                TargetType = TargetType.Group,
                            };
                            invList.Add(igerDTO);
                        }
                    }
                }
                
                return invList;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}




