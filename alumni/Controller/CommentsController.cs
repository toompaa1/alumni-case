﻿using alumni.Models;
using alumni.Models.Dto.Comment;
using alumni.Services.CommentService;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using alumni.Models.Domain;
using Microsoft.AspNetCore.Cors;

namespace alumni.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CommentsController : ControllerBase
    {

        private readonly ICommentService _commentService;
        private readonly IMapper _mapper;

        public CommentsController(ICommentService commentService, IMapper mapper)
        {
            _commentService = commentService;
            _mapper = mapper;
        }
        [HttpGet("{commentId}")]
        public async Task<ActionResult<CommentReadDTO>> GetCommentById(int commentId)
        {

            if (!_commentService.CommentExist(commentId))
            {
                return NotFound();
            }
            var comment = await _commentService.GetCommentById(commentId);
            return _mapper.Map<CommentReadDTO>(comment);

        }

        [HttpGet("post/{postId}")]
        public async Task<ActionResult<IEnumerable<CommentReadDTO>>> GetAllCommentByPostId(int postId)
        {
            if (!_commentService.ParentPostExist(postId))
            {
                return NotFound();
            }

            var comments = await _commentService.GetAllCommentsByPostId(postId);
            return _mapper.Map<List<CommentReadDTO>>(comments);

        }

        [HttpPost]
        public async Task<ActionResult<CommentReadDTO>> CreateComment(CommentCreateDTO commentCreateDTO)
        {

            var comment = _mapper.Map<Comment>(commentCreateDTO);
            comment = await _commentService.CreateComment(comment);

            return _mapper.Map<CommentReadDTO>(comment);

        }

        [HttpPut("{commentId}")]
        public async Task<ActionResult<CommentReadDTO>> UpdateComment(int commentId, CommentReadDTO commentReadDTO)
        {
            if (commentId != commentReadDTO.CommentId)
            {
                return BadRequest();
            }
            // need to get comment by id to no remove postId and userId
            if (!_commentService.CommentExist(commentId))
            {
                return NotFound();
            }

            var updatedComment = _mapper.Map<Comment>(commentReadDTO);
            var oldComment = await _commentService.GetCommentById(commentId);

            updatedComment.PostId = oldComment.PostId;
            updatedComment.UserId = oldComment.UserId;

            try
            {
                await _commentService.UpdateComment(updatedComment);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return _mapper.Map<CommentReadDTO>(updatedComment);
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteComment(CommentReadDTO commentReadDTO)
        {
            if (!_commentService.CommentExist(commentReadDTO.CommentId))
            {
                return BadRequest();
            }

            var comment = await _commentService.GetCommentById(commentReadDTO.CommentId);

            try
            {
                await _commentService.DeleteComment(comment);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok("Comment was deleted successfully");

        }


    }
}
