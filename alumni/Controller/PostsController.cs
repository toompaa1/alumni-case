﻿using alumni.Models.Domain;
using alumni.Services.PostService;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using alumni.Models.Dto.Post;
using System.Net.Mime;
using alumni.Models.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Cors;

namespace alumni.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PostsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPostService _postService;
        public PostsController(IMapper mapper, IPostService postService)
        {
            _mapper = mapper;
            _postService = postService;
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<IEnumerable<PostReadDto>>> GetPostsInGropusAndTopic(int userId)
        {
            var posts = await _postService.GetUserRelatedPosts(userId);
            return _mapper.Map<List<PostReadDto>>(posts);
        }

        [HttpGet("user/{userId}/{targetUserId}")]
        public async Task<ActionResult<IEnumerable<PostReadDto>>> GetDirectMessages(int userId, int targetUserId)
        {
            if (!_postService.Exist(targetUserId))
            {
                return NotFound("User does not exist");
            }
            var posts = await _postService.GetDirectMessages(userId, targetUserId);
            return _mapper.Map<List<PostReadDto>>(posts);
        }

        [HttpGet("group/{groupId}")]
        public async Task<ActionResult<IEnumerable<PostReadDto>>> GetPostsByGroup(int groupId)
        {
            var posts = await _postService.GetPostsByGoup(groupId);
            return _mapper.Map<List<PostReadDto>>(posts);
        }

        [HttpGet("topic/{topicId}")]
        public async Task<ActionResult<IEnumerable<PostReadDto>>> GetPostsByTopic(int topicId)
        {
            var posts = await _postService.GetPostsByTopic(topicId);
            return _mapper.Map<List<PostReadDto>>(posts);
        }

        [HttpGet("post/{postId}")]
        public async Task<ActionResult<PostReadDto>> GetPostById(int postId)
        {
            var post = await _postService.GetPostById(postId);
            if (post == null)
            {
                return NotFound();
            }
            return _mapper.Map<PostReadDto>(post);
        }

        [HttpGet("event/{eventId}")]
        public async Task<ActionResult<IEnumerable<PostReadDto>>> GetPostsByEvent(int eventId)
        {
            var posts = await _postService.GetPostsByEvent(eventId);
            return _mapper.Map<List<PostReadDto>>(posts);
        }

        [HttpPost]
        public async Task<ActionResult<PostReadDto>> CreatePost(PostCreateDTO postCreateDTO)
        {
            Post post = _mapper.Map<Post>(postCreateDTO);
            switch (postCreateDTO.TargetType)
            {
                case TargetType.Event:
                    {
                        post.EventId = postCreateDTO.TargetId;
                        break;
                    }
                case TargetType.Group:
                    {
                        post.GroupId = postCreateDTO.TargetId;
                        break;
                    }
                case TargetType.User:
                    {
                        post.CreatedByUserId = postCreateDTO.TargetId;
                        break;
                    }
                case TargetType.Topic:
                    {
                        post.TopicId = postCreateDTO.TargetId;
                        break;
                    }
                default:
                    {
                        return BadRequest("Not valid type");
                    }
            }
            try
            {
                post = await _postService.CreatePost(post);
            }
            catch
            {
                return BadRequest();
            }
            return _mapper.Map<PostReadDto>(post);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<PostReadDto>> UpdatePost(int id, PostUpdateDTO postUpdateDTO)
        {
            if (id != postUpdateDTO.Id)
            {
                return BadRequest("Ids not matching");
            }
            if (!_postService.PostExist(id))
            {
                return BadRequest("No existing post");
            }
            Post post = _mapper.Map<Post>(postUpdateDTO);
            post.PostId = postUpdateDTO.Id;
            try
            {
                await _postService.UpdatePost(post);
            }
            catch
            {
                return BadRequest("Database Error");
            }
            return NoContent();
        }
    }
}
