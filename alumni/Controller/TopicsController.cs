﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumni.Services.TopicService;
using AutoMapper;
using alumni.Models.Dto.Topic;
using alumni.Models.Domain;
using Microsoft.AspNetCore.Cors;
using alumni.Models.Domain.EF_ManyToMany;
using alumni.Models.Dto.User;
using Microsoft.AspNetCore.Authorization;

namespace alumni.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TopicsController : ControllerBase
    {
        private readonly ITopicService _topicService;
        private readonly IMapper _mapper;
        public TopicsController(ITopicService topicService, IMapper mapper)
        {
            _topicService = topicService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TopicReadDTO>>> GetAllTopics()
        {
            return _mapper.Map<List<TopicReadDTO>>(await _topicService.GetAllTopics());
        }

        [HttpGet("{topicId}")]
        public async Task<ActionResult<TopicReadDTO>> GetTopicById(int topicId)
        {
            Topic topic = await _topicService.GetTopicById(topicId);
            if (topic == null)
            {
                return NotFound();
            }
            return _mapper.Map<TopicReadDTO>(topic);
        }

        [HttpPost("{userId}")]
        public async Task<ActionResult<TopicReadDTO>> CreateTopic(TopicCreateDTO dtoTopic, int userId)
        {
            Topic topic = _mapper.Map<Topic>(dtoTopic);
            topic.UserId = userId;
            topic = await _topicService.CreateTopic(topic, userId);
            return CreatedAtAction("GetTopicById", new { topicId = topic.TopicId }, _mapper.Map<TopicReadDTO>(topic));
        }

        [HttpGet("{userId}/AllRelevantTopics")]
        public async Task<ActionResult<IEnumerable<TopicReadDTO>>> GetTopicByUserId(int userId)
        {
            var topic = await _topicService.GetTopicByUserId(userId);
            if (topic == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<TopicReadDTO>>(topic);
        }

        [HttpPost("join")]
        public async Task<ActionResult> JoinTopic(JoinTopicDTO joinTopicDTO)
        {
            TopicUser topicUser = _mapper.Map<TopicUser>(joinTopicDTO);
            if (!_topicService.TopicExist(joinTopicDTO.TopicId))
            {
                return NotFound("Topic does not exist.");
            }
            if (_topicService.UserExistsInTopic(joinTopicDTO.UserId, joinTopicDTO.TopicId))
            {
                return BadRequest("Already a member");
            }
            try
            {
                await _topicService.JoinTopic(topicUser);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok("Added to Topic.");

        }

        [HttpGet("{userId}/GetCreatedTopicsByUserId")]
        public async Task<ActionResult<IEnumerable<TopicReadDTO>>> GetCreatedTopicsByUserId(int userId)
        {
            var topic = await _topicService.GetCreatedTopics(userId);
            if (topic == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<TopicReadDTO>>(topic);
        }

        [HttpGet("{topicId}/users")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetAllTopicUsers(int topicId)
        {
            var _eventUsers = await _topicService.GetAllTopicUsers(topicId);
            if (_eventUsers == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<UserReadDTO>>(_eventUsers);
        }

    }
}